<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $subcats = DB::table('subcategories')

         ->leftjoin('servicetypes', 'subcategories.service_type','=','servicetypes.id')
         ->select('subcategories.*','servicetypes.type_name')
         ->get();
        return view('backend.subcategory.subcategories',compact('subcats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        //$cats = DB::table('categories')
        //->get();
        $types = DB::table('servicetypes')->get();
        return view('backend.subcategory.addsubcategory',compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return Input::all();
        DB::table('subcategories')->insert(
        [
            'sub_cat_name' => Input::get('sub_cat_name'),
            'sub_cat_status' => 1,
            'service_type' => Input::get('service_type'),
        ]
        );
         return redirect('subcategories')->with('success', 'New Sub Category Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cats = DB::table('categories')
        ->get();
         $sub_cat = DB::table('subcategories')
        ->where('subcategories.id',$id)
        ->leftjoin('categories','subcategories.cat_id','=','categories.id')
        ->select('subcategories.*', 'categories.cat_name')
        ->get();
        $subcat = $sub_cat[0];
        return view('backend.subcategory.editsubcategory',compact('cats','subcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('subcategories')->update(
        [
            'sub_cat_name' => Input::get('sub_cat_name'),
            'sub_cat_status' => 1,
            'cat_id' => Input::get('cat_id'),
        ]
        );
         return redirect('subcategories')->with('success', 'Sub Category updated Successfully');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('subcategories')->where('id', '=', $id)->delete();
        DB::table('services')->where('service_sub_cat_id', '=', $id)->delete();

        return redirect('subcategories')->with('success', 'Sub Category deleted Successfully');
    }
}
