<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = DB::table('abouts')
         ->get();
        return view('backend.about.abouts',compact('abouts'));
    }

   

    public function add()
    {
        return view('backend.about.addabout');
    }


    public function store(Request $request)
    {
        //return 'hy';
        DB::table('abouts')->insert(
        [
            'about_description' => Input::get('do'),
        ]
        );
         return redirect('abouts')->with('success', 'New data Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit()
    {

        $offer = DB::table('offers')
        //->where('id',$id)
        ->first();
        //return $cat;
        return view('backend.offer.editoffer',compact('offer'));
    }


    public function update(Request $request)
    {
        //return Input::get('offer_start');

        DB::table('offers')
            //->where('id', $id)
            ->update([
                     'offer_description' => Input::get('editor1'),
                     'offer_start' => date('d F, Y h:i a', strtotime(Input::get('offer_start'))),
                     'offer_end' => date('d F, Y h:i a', strtotime(Input::get('offer_end'))),
                     
                ]);

            return redirect('editoffer')->with('success', 'Offer Updated Successfully');


    }

  

    // public function destroy($id)
    // {
       

    //     DB::table('abouts')->where('id', $id)->delete();
       

    //     return redirect('abouts')->with('success', 'Data Deleted Successfully');
    // }
}
