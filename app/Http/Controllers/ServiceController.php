<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class ServiceController extends Controller
{
  
    
    
    
    
    public function index()
    {
        $services = DB::table('services')
                    ->leftjoin('servicetypes','services.service_type_id','=','servicetypes.id')
                    ->select('services.*','servicetypes.type_name')
                    ->get();
        return view('backend.service.services',compact('services'));
    }

  
    
    
    
    
    
    
    public function add()
    {
        $servicetypes = DB::table('servicetypes')->get();
        return view('backend.service.addservice',compact('servicetypes'));
    }

    
    
    
    
    
    
    
    public function store(Request $request)
    {
        $imgname = Input::get('name');
        $filename = time().'.jpg';

        // if(Input::has('name'))
        // {
        //     return "hy";
        //     
       
            
        //     //return $filename;
        // }
        Image::make(Input::file('name'))->save('public/uploads/service/'.$filename);

        DB::table('services')->insert(
        [
            'service_title' => Input::get('service_title'),
            'service_type_id' => Input::get('service_type_id'),
            'service_image' => $filename,
            'service_description' => Input::get('editor1'),
            'service_status' => 1,
        ]
        );
         return redirect('services')->with('success', 'New Service Added Successfully');
    }

    
    
    
    
    
    
     public function view( \SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, $id)
    {
         $services = DB::table('services')
                    ->where('services.id',$id)
                    ->leftjoin('servicetypes','services.service_type_id','=','servicetypes.id')
                    ->select('services.*','servicetypes.type_name')
                    ->get();
        $servicetypes = DB::table('servicetypes')->get();
        $service = $services[0];
        
         
          
        session(['service_id' => $service->id]);
        session(['service_title' => $service->service_title]);
         
        // if(!session_id()) {
        //     session_start();
        //     //$_SESSION["post_id"] = $cat->id;
        //     session(['wed' => '']);
        //     session(['wed' => $service->id]);
        // }
        
        
        
        
      
         $login_link = $fb
            ->getRedirectLoginHelper()
            ->getLoginUrl('http://dashboard.redeleganceevents.com/service_callback', ['email','user_events','manage_pages']);
           // ->getLoginUrl('localhost/red_done_n/service_callback', ['email', 'user_events']);
        
         
        return view('backend.service.singleservice',compact('service','servicetypes','login_link'));
    }

    
    
    
    
    
    
    public function edit($id)
    {
        $services = DB::table('services')
                    ->where('services.id',$id)
                    ->leftjoin('servicetypes','services.service_type_id','=','servicetypes.id')
                    ->select('services.*','servicetypes.type_name')
                    ->get();
        $servicetypes = DB::table('servicetypes')->get();
        $service = $services[0];
        
       
        
        return view('backend.service.editservice',compact('service','servicetypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('services')
            ->where('id', $id)
            ->update([
                     'service_title' => Input::get('service_title'),
            'service_type_id' => Input::get('service_type_id'),
            'service_description' => Input::get('editor1'),
            'service_status' => 1,
                ]);
            if(Input::file('name'))
            {
                //return 'hy';
                 $filename = time().'.jpg';

                 Image::make(Input::file('name'))->save('public/uploads/service/'.$filename);
                   DB::table('services')
            ->where('id', $id)
            ->update([
                    
                    'service_image' => $filename,
                    
                ]);

            }

            return redirect('services')->with('success', 'Service Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ser = DB::table('services')->where('id', $id)->delete();
        //$image = $ser
        //if()


        return redirect('services')->with('success', 'Service removed Successfully');
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function service_callback(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
    {
           
        // if(!session_id()) {
        //     session_start();
            
        // }
         
   
         $post_id =  session('service_id');
         $post_title =  session('service_title');
       
        // $FbAppInfo = new FbAppInfo();
        //         $fb = $FbAppInfo->GetFbAppInfo();
        
        $helper = $fb->getRedirectLoginHelper();
        
        try {
          $accessToken = $helper->getAccessToken();
          //echo $accessToken; exit;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
         
            return redirect()->back()->with('danger',$e->getMessage());
            
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          return redirect()->back()->with('danger',$e->getMessage());
        }
        
        if (! isset($accessToken)) {
          if ($helper->getError()) {
            header('HTTP/1.0 401 Unauthorized');
//            echo "Error: " . $helper->getError() . "\n";
//            echo "Error Code: " . $helper->getErrorCode() . "\n";
//            echo "Error Reason: " . $helper->getErrorReason() . "\n";
         //echo "Error Description: " . $helper->getErrorDescription() . "\n";
              return redirect()->back()->with('danger',$helper->getErrorDescription());
          } else {
            header('HTTP/1.0 400 Bad Request');
            return redirect()->back()->with('danger','Bad Request');
          }
         // exit;
        }
        
        // Logged in
        
       // echo '<h3>Access Token</h3>';
       // var_dump($accessToken->getValue());
        
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        
        
       
        //echo '<h3>Metadata</h3>';
       // var_dump($tokenMetadata);
        
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('209983389429145'); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();
        
        if (! $accessToken->isLongLived()) {
          // Exchanges a short-lived access token for a long-lived one
          try {
            $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
          } catch (Facebook\Exceptions\FacebookSDKException $e) {
            
              return redirect()->back()->with('danger',$e->getMessage());
           
              
            //  echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
           // exit;
          }
        
          echo '<h3>Long-lived</h3>';
          //$access = $accessToken->getValue()
            
         // var_dump($accessToken->getValue());
        }
        
        $_SESSION['fb_access_token'] = (string) $accessToken;
        
        // User is logged in with a long-lived access token.
        // You can redirect them to a members-only page.
        //header('Location: https://example.com/members.php');
        
        
        
         // define your POST parameters (replace with your own values)
                $params = array(
                  "access_token" => $accessToken, // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
                  "message" =>  $post_title,
                  "link" => "http://dashboard.redeleganceevents.com/service_callback/".$post_id,
                
                  "name" => "Shobarjonnoweb",
                 
                 
                );
                 
                // post to Facebook
                // see: https://developers.facebook.com/docs/reference/php/facebook-api/
                try {
                  $ret = $fb->post('/250434772134371/feed', $params);
                  return redirect()->back()->with('success','Successfully posted to Facebook');
                } catch(Exception $e) {
                  //echo $e->getMessage();
                  return redirect()->back()->with('danger',$e->getMessage());
                }
        
        

    }


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
