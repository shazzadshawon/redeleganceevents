<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    
    public function home(Request $request)
    {
        
        
        if(session('ip') != \Request::ip())
        {
             $today = date("y-m-d");
            //return session('ip');
            $ip = \Request::ip();
            $request->session()->put('ip', \Request::ip());
            $counter = DB::table('visitors')->first();
            $hit = $counter->counter;
            $daily = $counter->daily_count;
            $update_time = $counter->updated_at;

            $visit = DB::table('visitors')->first();
//var_dump($visit);exit;


            $location_details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            
             $region = explode(" ",$location_details->region);
             $details = strtolower($region[0]);
            
            $newCount = DB::table('visitors')->update(
                [
                    'counter'       => $hit+1,
                    'daily_count' => $daily+1,
                    // 'updated_at'    => $date,

                ]);
            
            if ($update_time != $today) {
                //echo $update_time." ".$today;
                DB::table('visitors')->update(
                [
                    
                    'daily_count' => 1,
                    'updated_at'    => $today,

                ]);
            }
           // $details = $details_location->region;
            

            if($details == 'barisal')
            {
                DB::table('visitors')->update(
                [                 
                    'barisal' => $visit->barisal + 1,
                ]);
            }
            if($details == 'chittagong')
            {
                 DB::table('visitors')->update(
                [
                    'chittagong' => $visit->chittagong + 1,
                ]);

            }

            if($details == 'dhaka')
            {
                 DB::table('visitors')->update(
                [
                'dhaka' => $visit->dhaka + 1,
                ]);

            }

            if($details == 'khulna')
            {
                 DB::table('visitors')->update(
                [
                    'khulna' => $visit->khulna + 1,
                ]);
                
            }

            if($details == 'mymensingh')
            {
                 DB::table('visitors')->update(
                [
                    'mymensingh' => $visit->mymensingh + 1,
                ]);

            }

            if($details == 'rajshahi')
            {
                 DB::table('visitors')->update(
                [
                    'rajshahi' => $visit->rajshahi + 1,
                ]);
                
            }

            if($details == 'rangpur')
            {
                 DB::table('visitors')->update(
                [
                    'rangpur' => $visit->rangpur + 1,
                ]);
                
            }

            if($details == 'sylhet')
            {
                 DB::table('visitors')->update(
                [
                    'sylhet' => $visit->sylhet + 1,

                ]);
                
            }



        }
        
        
        
        //return \Request::ip();
        $sliders = DB::table('sliders')->get();
        $services = DB::table('services')->get();
        $reviewsObj = DB::table('reviews')->get();
        $members = DB::table('teams')->get();
        $offer = DB::table('offers')->first();
        //$reviews = array();
        $reviews = array();

        foreach ($reviewsObj as $obj) {
            $reviews[] = (array)$obj;
        }
        
 // echo "<pre>";
 //         print_r($reviews);
 //         exit;
//return $reviews;
        return view('frontend.home',compact('sliders','services','reviews','members','offer'));
    }

    public function form()
    {
        $filename ='';
        return view('form',compact('filename'));
    }
    public function upload()
    {
        // $img = Image::make('uploads/foo.jpg');
        // // get file size
        // $size = $img->filesize();
        //return time();
        $filename = time();
        // return (Input::get('name'));
        // return $ext = pathinfo(Input::file('name'), PATHINFO_EXTENSION);

        Image::make(Input::file('name'))->resize(300, 150)->opacity(50)->save('uploads/'.$filename.'.jpg');
        return view('form',compact('filename'));
        //return Input::get('name');
    }

     public function about()
    {
        $abouts = DB::table('abouts')->get();
        //$reviews = array();
        return view('frontend.about',compact('abouts'));
    }     
    public function service($id)
    {
        $service = DB::table('services')->where('id',$id)->first();
        return view('frontend.service',compact('service'));
    }    
    public function service_cat($id)
    {
        $services = DB::table('services')
        ->where('service_type_id',$id)
        ->get();

        $type = DB::table('servicetypes')
            ->where('id',$id)
            ->first();
        return view('frontend.service_cat',compact('services','type'));
    }    
     public function allpackage()
    {
        $packages = DB::table('packages')->get();
        return view('frontend.allpackage',compact('packages'));
    }    
     public function singlepackage($id)
    {
        $package = DB::table('packages')->where('id',$id)->first();
        return view('frontend.singlepackage',compact('package'));
    }     
    public function gallery()
    {
         $galleries = DB::table('galleries')->get();
         //return count($galleries);
        return view('frontend.gal',compact('galleries'));
    }     
    public function contactus()
    {
        return view('frontend.contactus');
    }    
    public function storecontact()
    {
        //return Input::all();
        DB::table('contacts')
        ->insert(
            [
                'contact_title' => Input::get('contact_title'),
                'contact_email' => Input::get('contact_email'),
                'contact_phone' => Input::get('contact_phone'),
                'contact_description' => Input::get('contact_description'),
            ]
            );
        return redirect()->back()->with('success','Message Sent Successfully');
    }    
    


}
