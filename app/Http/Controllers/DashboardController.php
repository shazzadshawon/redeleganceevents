<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    
    public function location($ip)
    {
        //$ip = \Request::ip();
            $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            $reg = explode(" ",$details->region);
            echo "<pre>";
           // print_r(strtolower($reg[0]));
            print_r($details);
            exit;
    }

    
    public function index(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb1)
    {
        
        
// Event line Chart
  
          $dates = DB::table('events')->distinct()
          ->where('created_at','>',date("Y-m-d"))
          ->orderby('created_at','asc')->get(['created_at']);
          $dateArray  = array();
          $countArray = array();
          $i=0;

          foreach ($dates as $date) {
            if ($i>9) {
                break;
            }
              $dateArray[] = $date->event_date;
                $date_count = DB::table('events')->where('created_at', $date->event_date )->count();
                $countArray[] = $date_count;
                $i++;
          }
          //return $countArray;
// lineChartTest

            $postjs = app()->chartjs
                     ->name('barChartTest')
                     ->type('line')
                     ->size(['width' => 100, 'height' => 45])
                     ->labels($dateArray)
                     ->datasets([
                         [
                             "label" => "Events per day",
                             'backgroundColor' => 'skyblue',
                             'data' => $countArray
                         ]

                   
                     ])
                     ->options([]);
                     

        
        
// visitor donat chart
                   
             $visitor = \DB::table('visitors')->first();
//print_r($visitor);
            $visitorArray = array();
            
            $visitorArray[] = $visitor->barisal;
            $visitorArray[] = $visitor->chittagong;
            $visitorArray[] = $visitor->dhaka;
            $visitorArray[] = $visitor->khulna;
            $visitorArray[] = $visitor->mymensingh;
            $visitorArray[] = $visitor->rajshahi;
            $visitorArray[] = $visitor->rangpur;
            $visitorArray[] = $visitor->sylhet;
//print_r($visitorArray);
//exit;
            $visitorjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('doughnut')
                    ->size(['width' => 400, 'height' => 400])
                    ->labels(['Barisal','Chittagong','Dhaka','Khulna','Mymensingh','Rajshahi','Rangpur', 'Sylhet'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#f7970e', '#00b39a','#ff6664','#4caf50','#03a9f4','#111317','#673ab7','#ff5722','purple'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB'],
                            'data' => $visitorArray
                        ]
                    ])
                    ->options([
                         'legend'=> [
                                'display'=> false,
                                'position'=> 'left',
                                'labels' => [
                                    'fontColor' => 'teal'
                                    ]
                                ]
                        ]);
        
        
        $app_token = '103949040329864|KKeWI2-Sxfusd4TeqpLpYACVxqQ';
        $fullresponse = $fb1->get('/1675468199374781/feed?fields=from,message,updated_time,comments,shares,reactions&limit=10', $app_token)->getDecodedBody();
        $response = $fullresponse['data'];
        
        
        
        return view('backend.dashboard',compact('postjs','visitorjs','response'));
    }

  

//  61ssTw)p2%%M red db pass
}
