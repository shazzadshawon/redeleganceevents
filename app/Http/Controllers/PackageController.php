<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $packages = DB::table('packages')
         ->leftjoin('categories', 'packages.cat_id','=','categories.id')
         ->select('packages.*','categories.cat_name')
         ->get();
        return view('backend.package.packages',compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $cats = DB::table('categories')
                ->get();
        return view('backend.package.addpackage',compact('cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return Input::all();
         DB::table('packages')->insert(
        [
            'package_name' => Input::get('package_name'),
            'package_subtitle' => Input::get('package_subtitle'),
            'package_description' => Input::get('editor1'),
            
            'package_include' => Input::get('editor2'),
            'package_exclude' => Input::get('editor3'),
            
        ]
        );
         return redirect('packages')->with('success', 'New Package Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //return
        $packages = DB::table('packages')
                    ->where('packages.id',$id)
                    ->leftjoin('categories','packages.cat_id','=','categories.id')
                    ->select('packages.*','categories.cat_name')
                    ->get();
                    $package = $packages[0];
        $cats = DB::table('categories')
                ->get();

                return view('backend.package.editpackage',compact('package','cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return Input::all();
        DB::table('packages')
            ->where('id', $id)
            ->update([
                    'package_name' => Input::get('package_name'),
                    'package_subtitle' => Input::get('package_subtitle'),
                    'package_description' => Input::get('editor1'),
                   
                    'package_include' => Input::get('editor2'),
                    'package_exclude' => Input::get('editor3'),
                ]);
             return redirect('packages')->with('success', 'Package Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('packages')->where('id', $id)->delete();
        return redirect('packages')->with('success', 'Package removed Successfully');
    }
}
