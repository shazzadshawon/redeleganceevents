@extends('layouts.frontend')

@section('content')
<div class="">
      <img style="height: 250px; width: 100%;" src="{{ asset('public/images/inner_bg.jpg') }}">
   </div>
    <!-- banner-slider -->
    <!-- breadcrumbs -->
    <div class="w3l_agileits_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a><span>«</span></li>
                <li><a href="{{ url('/allpackage') }}"> Packages</a><span>«</span></li>
                <li>{{$package->package_name}}</li>
            </ul>
        </div>
    </div>
<!-- //breadcrumbs -->
    
    <!--/story-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="speach">
                    <div class="resp-tabs-container">
                        <div class="tab1">
                            <div class="wthree_agile_tabs">
                                <div class="col-md-12">
                                    <div class="tab-info_text_agile_w3l">
                                        <h3>{{$package->package_name}} <small>{{$package->package_subtitle}}</small></h3>

                                        <?php print_r($package->package_description); ?>
                                        
                                    </div>
                                    <div class="gap"></div>
                                    <h2>Package Includes</h2>
                                    <ul class="list-group">
                                        <span class="list-group-item list-group-item-success">
                                        <?php print_r($package->package_include); ?>
                                        </span>
                                      
                                    </ul>
                                    
                                    <div class="gap"></div>
                                    <h2>Package Excludes</h2>
                                    <p><?php print_r($package->package_exclude); ?></p>
                                    
                                    <div class="gap"></div>
                                    <h2>Terms & Condition</h2>
                                    <ul class="list-group">
                                        <li class="list-group-item list-group-item-info">Only For Bangladeshi Passport Holder</li>
                                        <li class="list-group-item list-group-item-info">Rate Can Be Change Without Prior Notice</li>
                                        <li class="list-group-item list-group-item-info">Package Can Be Customized</li>
                                    </ul>
                                    
<!--
                                    <div class="well" style="font-weight:bold">
                                        Starting from 6500 bdt
                                    </div>
-->
                                </div>
                                <div class="clearfix"></div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
<!--//story-->
        
    <div class="gap"></div>
@endsection