<?php 
// echo "<pre>";
// print_r($reviews);
// exit();
//return $reviews[0];
 ?>


@extends('layouts.frontend')

@section('content') 

   <div class="w3ls_banner_section">
        <div class="snow-container">
            <div class="snow foreground"></div>
            <div class="snow foreground layered"></div>
            <div class="snow middleground"></div>
            <div class="snow middleground layered"></div>
            <div class="snow background"></div>
            <div class="snow background layered"></div>
        </div>
        <div class="callbacks_container">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
              @foreach($sliders as $slider)
                <li data-target="#myCarousel" data-slide-to="{{$slider->id}}" class=""></li>
              @endforeach
                <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li> -->
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
              @php
                $i=1;
              @endphp
                @foreach($sliders as $slider)
                <div class="@if($i==1) item active @else item @endif">
                  <img style="height: 600px" src="{{asset('public/uploads/slider/'.$slider->slider_image)}}" alt="Los Angeles">
                  <div class="carousel-caption banner_agile-info">
                    <h3>{{ $slider->slider_title }}</h3>
                    <p>{{ $slider->slider_subtitle }}</p>
                  </div>
                </div>
                @php
                  $i++;
                @endphp
                @endforeach

               <!--  <div class="item">
                  <img src="images/slider/11.jpg" alt="Chicago">
                  <div class="carousel-caption banner_agile-info">
                    <h3>Your Event Patner</h3>
                    <p>Dummy Text</p>
                  </div>
                </div>

                <div class="item">
                  <img src="images/slider/44.jpg" alt="New York">
                  <div class="carousel-caption banner_agile-info">
                    <h3>Your Imagine We Create</h3>
                    <p>Dummy Text</p>
                  </div>
                </div> -->
              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            <!-- <ul class="rslides" id="slider3">
                <li>
                
                      <div class="banner_agile-info">
                          <h3>We Make Your Dream True</h3>
                          <p>CREATIVE WEDDING PLANNER</p>
                           <div class="banner-mid-wthree"> <a href="index.html" class="hvr-buzz"> <img src="images/couple.png" alt="wp"/></a> </div>
                      </div>
                </li>
                <li>
                      <div class="banner_agile-info">
                          <h3>We are Getting Married</h3>
                          <p>CREATIVE WEDDING PLANNER</p>
                           <div class="banner-mid-wthree"> <a href="index.html" class="hvr-buzz"> <img src="images/couple.png" alt="wp"/></a> </div>
                      </div>
                </li>
                <li>
                    <div class="banner_agile-info">
                      <h3>We Make Your Dream True</h3>
                      <p>CREATIVE WEDDING PLANNER</p>
                       <div class="banner-mid-wthree"> <a href="index.html" class="hvr-buzz"> <img src="images/couple.png" alt="wp"/></a> </div>
                    </div>
                </li>
            </ul> -->
            <div class="clearfix"></div>
        </div>
    </div>
            <!--/story-->
                <div class="about_section">
                    <div class="container">
                           <div class="wthree_title_agile">
                                <h2>Our <span>Events</span></h2>
                                <p><i class="fa fa-calendar" aria-hidden="true"></i></p>
                                
                            </div>
                         <p class="sub_para">Corporate and Wedding</p>
                         <div class="inner_w3l_agile_grids">
                                <!-- Bottom to top-->
                                    <div class="col-md-6 team-grid">
                                        <!-- normal -->
                                        <a href="{{url('service_category/3')}}">
                                            <div class="ih-item circle effect10 bottom_to_top">
                                                <div class="img"><img src="{{asset('public/images/corporate.jpg')}}" alt="img" /></div>
                                                <div class="info">
                                                    <h3>Corporate</h3>
                                                    
                                                </div>
                                            </div>
                                        </a>
                                        <!-- end normal -->
                                          <h4>Corporate</h4>
                                          <p>Corporate event can be defined as a gathering that is sponsored by a business for its employees, business partners, clients and/or prospective clients. These events .. </p>
                                          <!-- <div class="icons">
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li class="team-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                                    </ul>
                                                </div> -->
                                    </div>
                                    <div class="col-md-6 team-grid">
                                        <!-- normal -->
                                        <a href="{{url('service_category/1')}}">
                                            <div class="ih-item circle effect10 bottom_to_top">
                                                <div class="img"><img src="{{asset('public/images/a2.jpg')}}" alt="img" /></div>
                                                <div class="info">
                                                    <h3>Wedding</h3>

                                                </div>
                                            </div>
                                        </a>
                                        <!-- end normal -->
                                         <h4>Wedding</h4>
                                          <p>Wedding stage decoration may contain numerous elements such as traditional flowers, oriental flowers, standees and props, curtains, drapes, crystals, decorative stones and mirrors and other soft decorative elements. Each wedding .. </p>
                                          <!-- <div class="icons">
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li class="team-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                                    </ul>
                                                </div> -->
                                    </div>
                                    <div class="clearfix"></div>
                        </div>
                    </div>
                    
  
                </div>
            <!--//story-->
            
            <!--/counter-->
            <!--
            <div class="agileinfo_counter_section">
                <div class="wthree_title_agile">
                    <h3 class="h-t">Wedding <span>Coming Soon</span></h3>
                    <p><i class="fa fa-heart-o" aria-hidden="true"></i></p>

                </div>
                <p class="sub_para two">WE ARE GETTING MARRIED</p>
                <div class="wthree-counter-agile">
                    <section class="examples">
                    <div class="simply-countdown-losange" id="simply-countdown-losange"></div>
                    <div class="clearfix"></div>
                    </section>                    
                </div>
                <div class="clearfix"></div>
            </div>
            <!--//Team-->
            <div class="banner-bottom">
                <!--//screen-gallery-->
                <div class="wthree_title_agile">
                    <h3>Team <span>Members</span></h3>
                    <!-- <p><i class="fa fa-heart-o" aria-hidden="true"></i></p> -->
                </div>
                    <!-- <p class="sub_para">WE ARE GETTING MARRIED</p> -->
                    <div class="inner_w3l_agile_grids">
                        <div class="sreen-gallery-cursual">
                           <div id="owl-demo" class="owl-carousel">
                           @foreach($members as $member)
                                <div class="item-owl">
                                    <div class="test-review">
                                      <img style="height: 200px" src="{{asset('public/uploads/team/'.$member->team_image)}}" class="img-responsive" alt=""/>
                                      <h5><?php print_r($member->team_title) ?></h5>
                                      <h4><?php print_r($member->team_description) ?></h4>
                                    </div>
                                </div>
                           @endforeach
                          </div>
                        <!--//Teams-->
                </div>
            </div>
            </div>
            <!-- /property-grids -->
            <div class="property-grids">
                 <div class="agile-homes-w3l  grid">
                        <a href="{{url('service_category/2')}}">
                            <div class="col-md-6 home-agile-left">
                                <figure class="effect-moses">
                                    <img src="{{ asset('public/images/bithday.png') }}" alt="" />
                                    <figcaption>
                                        <h4>Birthday Services</h4>
                                        <p>You Imagine We Create</p>
                                    </figcaption>           
                                </figure>
                            </div>
                        </a>
                        
                        <a href="{{url('service_category/2')}}">
                            <div class="col-md-6 home-agile-text">
                               <h4>Music and Sound System</h4>
                               <p>We have high range Orginal JBL, QW4, SRX, SUB etc sound box collection. We also have Disco LED lighting system.</p>
                                <!-- <div class="date">
                                   <h5><i class="fa fa-calendar" aria-hidden="true"></i> March 20 2017</h5>
                                   <h5><i class="fa fa-clock-o" aria-hidden="true"></i> 10.00am to 12.00pm</h5>
                                </div> -->
                                <div class="icon_wthree"><i class="fa fa-music" aria-hidden="true"></i></div>
                            </div>
                        </a>
                        <div class="clearfix"></div>    

                        <div class="col-md-6 home-agile-text">
                           <h4>Offers</h4>
                           <?php print_r($offer->offer_description) ?>
                           <div class="date">
                               <h5><i class="fa fa-clock-o" aria-hidden="true"></i>{{$offer->offer_start}}</h5> To
                               <h5><i class="fa fa-clock-o" aria-hidden="true"></i> {{$offer->offer_end}}</h5>
                           </div>
                           <div class="icon_wthree"><i class="fa fa-cutlery" aria-hidden="true"></i></div>

                        </div>
                        <a href="{{url('service_category/4')}}">
                            <div class="col-md-6 home-agile-left">
                                <figure class="effect-moses">
                                    <img src="{{ asset('public/images/travel.jpg') }}" alt="" />
                                <figcaption>
                                    <h4>Ticketing Services</h4>
                                    <p>Air Ticketing & Packeg Tour</p>
                                </figcaption>           
                                </figure>
                            </div>
                        </a>
                        <div class="clearfix"></div>    
                 </div>
            </div>
            <!-- //property-grids -->
    
            <!--//Service sub catagories-->
            <div class="banner-bottom">
                <!--//screen-gallery-->
                <div class="wthree_title_agile">
                    <h3>Service <span>Sub Catagories</span></h3>
                    <!-- <p><i class="fa fa-heart-o" aria-hidden="true"></i></p> -->
                </div>
                    <!-- <p class="sub_para">WE ARE GETTING MARRIED</p> -->
                    <div class="inner_w3l_agile_grids">
                        <div class="sreen-gallery-cursual">
                           <div id="owl-demo2" class="owl-carousel">
                           @foreach($services as $service)
                                <a href="{{url('service/'.$service->id)}}">
                                    <div class="item-owl">
                                        <div class="test-review">
                                          <img style="height: 150px" src="{{asset('public/uploads/service/'.$service->service_image)}}" class="img-responsive" alt=""/>
                                          <h5>{{ $service->service_title }}</h5>                                          
                                        </div>
                                    </div>
                                </a>
                           @endforeach
                             
                          </div>
                </div>
            </div>
            </div>
            <!--//Service sub catagories-->
            

            <!-- /Reviews-->
           <div class="wthree-news text-center">
        <div class="container">
          <div class="wthree_title_agile">
                    <h3><span>Reviews</span></h3>
                <p><i class="fa fa-heart-o" aria-hidden="true"></i></p>
                
              </div>
             <p class="sub_para">OUR CLIENT REVIEWS</p>
             <div class="inner_w3l_agile_grids">
          <div class="col-md-6 wthree-right">
            <div class="w3-agile-left">
              <div class="work-left">
                <h4><?php print_r($reviews[0]['created_at']) ?></h4>
              </div>
              <div class="agileits-right text-left">
                <a href="single.html"><?php print_r($reviews[0]['review_title']) ?></a>
                <p><?php print_r($reviews[0]['review_description']) ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="wel-right">
              <div class="content-item item-image1">
                 <a href=""><img style="width: 350px" src="{{asset('public/uploads/review/'.$reviews[0]['review_image'])}}" alt="" /></a>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-6 wthree-right no-marg">
            <div class="wel-right2">
              <div class="content-item item-image2">
                 <a href=""><img style="width: 350px" src="{{asset('public/uploads/review/'.$reviews[1]['review_image'])}}" alt="" /></a>
              </div>
            </div>
            <div class="w3-agile-left2">
              <div class="work-left">
                <h4><?php print_r($reviews[1]['created_at']) ?></h4>
              </div>
              <div class="agileits-right text-left">
                  <a href="single.html"><?php print_r($reviews[1]['review_title']) ?></a>
                <p><?php print_r($reviews[1]['review_description']) ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          <div class="col-md-6 wthree-right">
            <div class="w3-agile-left">
              <div class="work-left">
                <h4><?php print_r($reviews[2]['created_at']) ?></h4>
              </div>
              <div class="agileits-right text-left">
                  <a href="single.html"><?php print_r($reviews[2]['review_title']) ?></a>
                <p><?php print_r($reviews[2]['review_description']) ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="wel-right ">
              <div class="content-item item-image1">
                 <a href=""><img style="width: 350px" src="{{asset('public/uploads/review/'.$reviews[2]['review_image'])}}" alt="" /></a>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-6 wthree-right no-marg">
            <div class="wel-right2">
              <div class="content-item item-image2">
                <a href=""><img style="width: 350px" src="{{asset('public/uploads/review/'.$reviews[3]['review_image'])}}" alt="" /></a>
              </div>
            </div>
            <div class="w3-agile-left2">
              <div class="work-left">
                <h4><?php print_r($reviews[3]['created_at']) ?></h4>
              </div>
              <div class="agileits-right text-left">
                   <a href="single.html"><?php print_r($reviews[3]['review_title']) ?></a>
                <p><?php print_r($reviews[3]['review_description']) ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
            <!-- //Events-->
@endsection