@extends('layouts.frontend')

@section('content')

	<!-- banner-slider -->
    
{{-- w3ls_banner_section second --}}
<div class="">
      <img style="height: 250px; width: 100%;" src="{{ asset('public/images/inner_bg.jpg') }}">
   </div>
	<!-- breadcrumbs -->
	<div class="w3l_agileits_breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="{{url('/')}}">Home</a><span>«</span></li>
				<li>About Us</li>
			</ul>
		</div>
	</div>
<!-- //breadcrumbs -->

<!--/story-->
    <div class="w3l_inner_section about">
        <div class="container">
               <div class="wthree_title_agile">
                    <h2>Our <span>Story</span></h2>
                </div>

            <div class="col-md-12">
                <div class="tab-info_text_agile_w3l">
                    
                    <p>RED ELEGANCE is the one stop solution for any types of event service. RED ELEGANCE Event Management is a renowned target oriented venture run by a group of skilled and dedicated personnel who has the experience of organizing a numerous illustrious corporate and wedding events. This organization carries a vivid fame and glory by an incompatible performance in Wedding planning and Corporate Event management services since the establishment. Customer’s satisfaction is the highest priority of our service. We not only provide services for wedding and corporate event but also care for the quality. Our professional honesty, sincerity and quality control ensure you a splendid wedding planning and corporate event management which is unique throughout the country.</p>
                </div>
            </div>
        </div>

        <div class="gap"></div>
        <div class="container mission">
               <div class="wthree_title_agile">
                    <h2>Our <span>Mission</span></h2>
                </div>

            <div class="col-md-12">
                <div class="tab-info_text_agile_w3l">
                    
                    <p>Our mission of Trendy Events Services is to provide the highest level of event planning services while maintaining extraordinary customer service. We understand how important and personal your occasion is so we know it’s vital to choose the right Event Coordinator to handle it for you. To put it simple, we love planning corporate events and parties and we are brilliant at it.</p>
                </div>
            </div>
        </div>

        <div class="gap"></div>
        
        <div class="container vision">
               <div class="wthree_title_agile">
                    <h2>Our <span>Vision</span></h2>
                </div>

            <div class="col-md-12">
                <div class="tab-info_text_agile_w3l">
                    
                    <div class="well">
                        <strong>
                        To challenge our skills and abilities, and create an event management platform that’s strong, dependable and intuitive, allowing event professionals to work efficiently and effortlessly
                        </strong>
                    </div>
                    
                    <div class="well">
                        <strong>
                            To keep people of all ages and abilities entertained, and provide a moment to last a lifetime
                        </strong>
                    </div>
                    
                    <div class="well">
                        <strong>To be recognized by our customers, employees, vendors and partners as the market leading event management organizer in the Bangladesh.</strong>
                    </div>
                </div>
            </div>
        </div>

        <div class="gap"></div>
        <div class="container value">
               <div class="wthree_title_agile">
                    <h2>Our <span>Values</span></h2>
                </div>

            <div class="col-md-12">
                <div class="tab-info_text_agile_w3l">
                    
                    <p>We are in the business of the most invaluable out put a man has ever producing the idea. We believe the potential of our agency has no limit and is driven by our associates and their imagination.</p>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">Quality</div>
                        <div class="panel-body">
                            Everything projects we carry out is a "fit for purpose" second best is not an option. As a business, and professionals who have been in the field for years, we know what kind of services our customers expect and we are here to make a difference. We are constantly driven by our customers and their requirements. In order to achieve this goal we ensure everyone operates at peak performance, not only by achieving our tasks, but to take it another step further and ensure full customer satisfaction by our after sale services. Compromising is not acceptable when it comes to quality at DNS. We set our standards high and then we aim to surpass them.
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">Pride</div>
                        <div class="panel-body">
                           We are a very passionate company and we strive to be the best in everything we do. This means we provide the highest possible level of service to our customers.
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">Trust</div>
                        <div class="panel-body">
                           Is integral to our relationships with our partners and our customers. Honesty is the best policy and we always deliver on the promises we make.
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">Achievement</div>
                        <div class="panel-body">
                          We strive to succeed, we enjoy what we are doing and we don't only work to meet your needs; we always try to exceed the expectation. We are firm believers that there is no end to the success that can be achieved within our company and yours. The more we achieve, the stronger we all become.
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">Prosperity</div>
                        <div class="panel-body">
                          All of our people take the opportunity to personally succeed and develop.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="gap"></div>
        
        <div class="container weDoCon">
               <div class="wthree_title_agile">
                    <h2>What <span>We Do</span></h2>
                </div>

            <div class="col-md-12">
                <div class="tab-info_text_agile_w3l">
                    
                    <p>Red Elegance is a one stop solution for all kinds of Event Management service. Now A Day's Event Management Service Are Become Popular Easy Readymade Service. We offer you A complete & Creative solution of your all kinds of Event Service.</p>
                    
                    <p>We offer the following services throughout everywhere in Bangladesh:</p>
                    
                    <div class="list-group weDo">

                    	
                    		
                    	@foreach($abouts as $wedo)
                    	<a href="" class="list-group-item"><?php print_r($wedo->about_description ) ?></a>
                    
                    	@endforeach
                       
                    </div>
                </div>
            </div>
         </div>
        
      </div>
<!--//story-->

<!-- /property-grids -->
<div class="property-grids">
    <div class="agile-homes-w3l  grid">
        <div class="col-md-12 home-agile-text">
            <h4>Message</h4>
            <p>I welcome you all website visitors and I hope that you enjoy browsing our website and find the information about our services you may required, Since We Plan entering the events management business in 2015 in the local Market. We Plan try to achieve their goals by providing quality services in the fields of corporate event management, wedding planning, marketing and branding. We arrived where we are today by placing our clients’ needs first. Their success is our success, and our commitment to their needs reaches through all levels of our company. That’s why, for instance, our high trained staff and representatives have developed their skills just to find the customer’s needs of events management and wedding planning in Bangladesh.</p>
            <div class="clearfix"></div>
            <div class="date">
               <h4>Md. Tanvir Karim</h4>
               <h5>Managing Director</h5>                             
               <h5>Red Elegance Events</h5>                             
            </div>
        </div>	
        <div class="clearfix"></div>	
     </div>
</div>
<!-- //property-grids -->

      <!-- /Events-->
			<div class="wthree-news text-center">
				<div class="container">
					<div class="wthree_title_agile">
						        <h3>Our <span>Logistics</span></h3>
								
							</div>						 
						 <div class="inner_w3l_agile_grids spa-agile">

							<div class="col-md-3 spa-grid">
								
									<i class="fa fa-paint-brush" aria-hidden="true"></i>
								
									<h4>Decorator and Flower</h4>
									<p>Mayer Doya Pushpo, JOY Decorator Service and Pushpo Nir.</p>
								
							</div>
							<div class="col-md-3 spa-grid">
								<i class="fa fa-music" aria-hidden="true"></i>
									<h4>Sound and Lighting</h4>
                                    <p>Vinyl Break, Sound 7, SAS, Tata sound system.</p>
							</div>
							<div class="col-md-3 spa-grid lost">
								
									<i class="fa fa-camera" aria-hidden="true"></i>
								
									<h4>Photography partners</h4>
									<p>Khandany photography, Bridal canvas, Wedding dairy & Level 3</p>
								
							</div>
							<div class="col-md-3 spa-grid lost">
								
								<i class="fa fa-birthday-cake" aria-hidden="true"></i>
								
								
									<h4>Balloons and palki and Tom Tom</h4>
									<p>Manik Tomtom, Dhaka tomtom</p>
							 
							</div>
							<div class="clearfix"> </div>
				         </div>
					</div>
		    </div>
	   <!-- //Events-->
@endsection