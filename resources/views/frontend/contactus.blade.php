




@extends('layouts.frontend')

@section('content')

	<!-- banner-slider -->
<div class="">
	  <img style="height: 250px; width: 100%;" src="{{ asset('public/images/inner_bg.jpg') }}">
   </div>
	<!-- banner-slider -->
	<!-- breadcrumbs -->
	<div class="w3l_agileits_breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="{{url('/')}}">Home</a><span>«</span></li>
				<li>Contact</li>
			</ul>
		</div>
	</div>
	<!-- //breadcrumbs -->

			<!--/story-->
				<div class="w3l_inner_section">
					<div class="container">
						   <div class="wthree_title_agile">
						        <h3>Contact <span>Us</span></h3>
								<p><i class="fa fa-map-marker" aria-hidden="true"></i></p>
								
							</div>
						 <p class="sub_para">FIND OUR LOCATION</p>
						 <div class="inner_w3l_agile_grids">
						       <div class="w3ls_footer_grid">
									<div class="col-md-4 w3ls_footer_grid_left">
										<div class="w3ls_footer_grid_leftl">
											<i class="fa fa-map-marker" aria-hidden="true"></i>
										</div>
										<div class="w3ls_footer_grid_leftr">
											<h4>Location</h4>
											<p>127, Pir shaheb Goli,Shanti nagar,  Dhaka-1000</p>
										</div>
										<div class="clearfix"> </div>
									</div>
									<div class="col-md-4 w3ls_footer_grid_left">
										<div class="w3ls_footer_grid_leftl">
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</div>
										<div class="w3ls_footer_grid_leftr">
											<h4>Email</h4>
											<a href="mailto:info@example.com">admin@redElegance.com</a>
										</div>
										<div class="clearfix"> </div>
									</div>
									<div class="col-md-4 w3ls_footer_grid_left">
										<div class="w3ls_footer_grid_leftl">
											<i class="fa fa-phone" aria-hidden="true"></i>
										</div>
										<div class="w3ls_footer_grid_leftr">
											<h4>Call Us</h4>
											<p>+8801670726912</p>
											<p>+8801680683049</p>
											<p>+8801814454232</p>
										</div>
										<div class="clearfix"> </div>
									</div>
									<div class="clearfix"> </div>
								</div>
                               	<div class="w3_mail_grids">
								<form action="{{url('storecontact')}}" method="post">
								{{ csrf_field() }}
									<div class="col-md-6 w3_agile_mail_grid">
											<input type="text" placeholder="Your Name"  required="" name="contact_title">
											<input type="email" placeholder="Your Email" required="" name="contact_email">
											<input type="text" placeholder="Your Phone Number" required="" name="contact_phone">

										
									</div>
									<div class="col-md-6 w3_agile_mail_grid">
										<textarea placeholder="Your Message" required="" name="contact_description"></textarea>
										<input type="submit" value="Submit">
									</div>
									<div class="clearfix"> </div>
								</form>
							</div>
					   </div>
							<div class="map">
								<iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d49128.93509780578!2d90.3862384085345!3d23.764135941573404!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3755b87c99bfe3e9%3A0xfda230c87019af5!2sredelegance+dhaka!3m2!1d23.7525893!2d90.41671869999999!5e0!3m2!1sen!2s!4v1499520801950" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
					

			<!--//story-->


@endsection