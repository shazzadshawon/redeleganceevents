@extends('layouts.frontend')

@section('content')

 <script src="{{ asset('public/js/frontend/lsb.min.js') }}"></script>
			<script>
			$(window).load(function() {
				$('.gallery_loader').show();
				$('.loader_image').hide();
				$.fn.lightspeedBox();
			});
			</script>
	<!-- banner-slider -->
	
{{-- w3ls_banner_section second --}}
<div class="">
	  <img style="height: 250px; width: 100%;" src="{{ asset('public/images/inner_bg.jpg') }}">
   </div>
	<!-- banner-slider -->
	<!-- breadcrumbs -->
	<div class="w3l_agileits_breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="{{url('/')}}">Home</a><span>«</span></li>
				<li>Gallery</li>
			</ul>
		</div>
	</div>
	<!-- //breadcrumbs -->

	<!--/story-->
	<div class="w3l_inner_section">
		<div class="container">
		    <div class="wthree_title_agile">
		        <h2>Gallery</h2>
				<p><i class="fa fa-television" aria-hidden="true"></i></p>
			</div>
			<img class="img-responsive loader_image" src="{{asset('public/images/page_load.gif')}}" style="margin: 0 auto">
			<div class="gallery_loader" style="display: none">
			@for ($i = 0; $i < count($galleries); $i=$i+3)
				<div class="row">
				{{-- grid 1 --}}
					<div class="inner_w3l_agile_grids">	
						@if (!empty($galleries[$i]))
						<div class="col-md-4 w3_tabs_grid">
							<div class="grid">
								<a href="{{asset('public/uploads/gallery/'.$galleries[$i]->gallery_image)}}" class="lsb-preview" data-lsb-group="header">
									<figure class="effect-winston">
										<img src="{{asset('public/uploads/gallery/'.$galleries[$i]->gallery_image)}}" class="img-responsive" alt=" " />
										<figcaption>
											<p>
												<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
												<span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
												<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
											</p>
										</figcaption>			
									</figure>
								</a>
							</div>
						</div>
						@endif
						
					</div>
					{{-- grid 2 --}}
					<div class="inner_w3l_agile_grids">

						@if (!empty($galleries[$i+1]))
						<div class="col-md-4 w3_tabs_grid">
							<div class="grid">
								<a href="{{asset('public/uploads/gallery/'.$galleries[$i+1]->gallery_image)}}" class="lsb-preview" data-lsb-group="header">
									<figure class="effect-winston">
										<img src="{{asset('public/uploads/gallery/'.$galleries[$i+1]->gallery_image)}}" class="img-responsive" alt=" " />
										<figcaption>
											<p>
												<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
												<span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
												<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
											</p>
										</figcaption>			
									</figure>
								</a>
							</div>
						</div>
						@endif
						
					</div>
					{{-- grid 3 --}}
					<div class="inner_w3l_agile_grids">
						@if (!empty($galleries[$i+2]))
						<div class="col-md-4 w3_tabs_grid">
							<div class="grid">
								<a href="{{asset('public/uploads/gallery/'.$galleries[$i+2]->gallery_image)}}" class="lsb-preview" data-lsb-group="header">
									<figure class="effect-winston">
										<img src="{{asset('public/uploads/gallery/'.$galleries[$i+2]->gallery_image)}}" class="img-responsive" alt=" " />
										<figcaption>
											<p>
												<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
												<span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
												<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
											</p>
										</figcaption>			
									</figure>
								</a>
							</div>
						</div>
						@endif
						
					</div>
					<div class="clearfix"> </div>
				</div>
			@endfor
				

				
			</div>
		</div>
	</div>
	<!--//story-->






	<script type="text/javascript" src="{{ asset('public/js/frontend/jquery-2.1.4.min.js') }}"></script><!-- Required-js -->

	<script src="js/responsiveslides.min.js"></script>
							<script>
								// You can also use "$(window).load(function() {"
								$(function () {
								  // Slideshow 4
								  $("#slider3").responsiveSlides({
									auto: true,
									pager:true,
									nav:false,
									speed: 500,
									namespace: "callbacks",
									before: function () {
									  $('.events').append("<li>before event fired.</li>");
									},
									after: function () {
									  $('.events').append("<li>after event fired.</li>");
									}
								  });
							
								});
							 </script>
							 <!-- js -->

	<!-- //main slider-banner --> 	
	
 <script src="{{ asset('public/js/frontend/lsb.min.js') }}"></script>
			<script>
			$(window).load(function() {
				$('.gallery_loader').show();
				$('.loader_image').hide();
				$.fn.lightspeedBox();
			});
			</script>
<!-- //js -->

	<!-- here stars scrolling icon -->
			<script type="text/javascript">
				$(document).ready(function() {
					/*
						var defaults = {
						containerID: 'toTop', // fading element id
						containerHoverID: 'toTopHover', // fading element hover id
						scrollSpeed: 1200,
						easingType: 'linear' 
						};
					*/
										
					$().UItoTop({ easingType: 'easeOutQuart' });
										
					});
			</script>
			<!-- start-smoth-scrolling -->
			<script type="text/javascript" src="{{ asset('public/js/frontend/move-top.js') }}"></script>
			<script type="text/javascript" src="{{ asset('public/js/frontend/easing.js') }}"></script>
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					$(".scroll").click(function(event){		
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
					});
				});
			</script>
			<!-- start-smoth-scrolling -->
	<!-- //here ends scrolling icon -->	
		<script src="{{ asset('public/js/frontend/bootstrap.min.js') }}"></script><!-- jQuery (necessary for Bootstrap's JavaScript plugins)


	<!--//counter-->
@endsection