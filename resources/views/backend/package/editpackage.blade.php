@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update Package</h4></div>
                 <div class="panel-body">
                     <div class="block">
                              
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('updatepackage/'.$package->id) }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Package Title</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="package_name" value="{{ $package->package_name  }}" />
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-md-2 control-label">Subtitle</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="package_subtitle"  value="{{ $package->package_subtitle }}"/>
                                        </div>
                                    </div>
                                   <!--  <div class="form-group">
                                        <label class="col-md-2 control-label">Category</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="cat_id">
                                            <option value="{{ $package->cat_id }}">{{ $package->cat_name }}</option>
                                                @foreach ($cats as $type)
                                                    <option value="{{ $type->id }}">{{ $type->cat_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> -->
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Package Description</label>
                                        <div class="col-md-10">
                                            <textarea class="form-control" name="editor1">
                                                <?php print_r($package->package_description) ?>
                                            </textarea>
                                        </div>
                                    </div>
                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Package Include</label>
                                        <div class="col-md-10">
                                            <textarea class="form-control" name="editor2">
                                                <?php print_r($package->package_include) ?>
                                            </textarea>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-md-2 control-label">Package Exclude</label>
                                        <div class="col-md-10">
                                             <textarea class="form-control" name="editor3">
                                                <?php print_r($package->package_exclude) ?>
                                            </textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="brn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection