@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update team</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                <h4>Add Form team</h4>
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('updateteammember/'.$team->id) }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Member Name</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="team_title" value="{{ $team->team_title }}" />
                                        </div>
                                    </div>
                                   

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">photo</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="team_image" value=""  autofocus>
                                        </div>
                                    </div>
              
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description </label>
                                        <div class="col-md-10">
                                            <div class="">
                                                <textarea class="form-control" name="team_description">{{ $team->team_description }}</textarea>
       
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="brn btn-success btn-lg" name="Submit" value="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection