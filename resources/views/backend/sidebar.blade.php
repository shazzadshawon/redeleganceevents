  <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="{{url('shobarjonnoweb')}}">Dashboard</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <?php 
                    $user = \Auth::User()->name;
                     ?>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                
                                 <span><i style="color: #ffffff; font-size: 100px" class="fa fa-user"></i></span>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">{{$user}}</div>
                                
                            </div>
                            <!-- <div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div> -->
                        </div>                                                                        
                    </li>
                    <!-- <li class="xn-title">Navigation</li> -->
                    <li  class="active">
                        <a href="{{ url('/shobarjonnoweb') }}"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>                    
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text"> Service </span></a>
                        <ul>
                            <li><a href="{{ url('addservice') }}"><span >Add Service</span></a></li>
                            <li><a href="{{ url('services') }}"><span class="">View All Services</span></a></li>
                            
                         <!--    <li><a href="{{ url('/addsubcategory') }}"><span class=""></span> Add Service sub-Category</a></li>
                            <li><a href="{{ url('/subcategories') }}"><span class=""></span> View Service sub-Categories</a></li>      -->    
                        </ul>
                    </li>
                   
                  <!--   <li class="xn-title">Components</li> -->
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Packages</span></a>                        
                        <ul>
                            <li><a href="{{ url('addpackage') }}"><span class="fa fa-heart"></span> Add Package</a></li>                            
                            <li><a href="{{ url('packages') }}"><span class="fa fa-cogs"></span> View All Packages</a></li>
                            <li><a href="{{ url('addpackagecategory') }}"><span class="fa fa-square-o"></span>  Add Package Category</a></li>                            
                            <li><a href="{{ url('packagecategories') }}"><span class="fa fa-pencil-square-o"></span> View Package Categories</a></li>
                           
                        </ul>
                    </li>  
                     <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Slider</span></a>
                        <ul>
                            <li><a href="{{ url('addslider') }}"> Add Slider Image</a></li>
                            <li><a href="{{ url('sliders') }}"> View Slider Images</a></li>
                        </ul>
                    </li>                  
                  
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-table"></span> <span class="xn-text">Gallery</span></a>
                        <ul>                            
                            <li><a href="{{ url('addgallery') }}"><span class="fa fa-align-justify"></span> Add Image</a></li>
                           
                            <li><a href="{{ url('galleries') }}"><span class="fa fa-download"></span> View All Images</a></li>                            
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-bar-chart-o"></span> <span class="xn-text">Team Member</span></a>
                        <ul>
                            <li><a href="{{ url('addteammember') }}"><span class="xn-text">Add new Member</span></a></li>
                            <li><a href="{{ url('teammembers') }}"><span class="xn-text">View All members</span></a></li>
                           
                        </ul>
                    </li>           
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">Review</span></a>
                        <ul>
                            <li><a href="{{ url('addreview') }}"><span class="xn-text">Add Review</span></a></li>
                            <li><a href="{{ url('reviews') }}"><span class="xn-text">View All Reviews</span></a></li>
                        </ul>
                    </li>                    
                    <li>
                        <a href="{{ url('abouts') }}"><span class="fa fa-download"></span> <span class="xn-text">About</span></a>
                    </li>     
                     <li>
                        <a href="{{ url('messages') }}"><span class="fa fa-map-marker "></span> <span class="xn-text">View Message</span></a>
                    </li>                    
                    <li>
                        <a href="{{ url('editoffer') }}"><span class="fa fa-map-marker "></span> <span class="xn-text">Current Offer</span></a>
                    </li>                    
                   
                    
                </ul>