@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update Gallery Image</h4></div>
                 <div class="panel-body">
                     <div class="block">
                               
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('updategallery/'.$gallery->id) }}"  enctype="multipart/form-data">      
                                    {{ csrf_field() }}                             
                                   
                                   

                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="gallery_image" value=""  autofocus>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-md" name="Submit" />
                                        </div>
                                    </div>
              
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> </label>
                                        <div class="col-md-10">
                                            <div class="">
                                               <img style="width: 100%;" src="{{ asset('public/uploads/gallery/'.$gallery->gallery_image) }}">
       
                                            </div>
                                        </div>
                                    </div>

                                 


                                   

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection