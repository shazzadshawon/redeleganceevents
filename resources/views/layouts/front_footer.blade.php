  <div class="agile-footer">
		<div class="container">
			<div class="aglie-info-logo">
			
				<div class="banner-mid-wthree"> <a href="{{url('/')}}" class="hvr-buzz"> <img src="{{asset('public/images/logo_c.png')}}" alt="wp"/></a> </div>
			</div>
			<ul class="aglieits-nav">
				<li><a href="{{url('/')}}">Home</a></li>
				<li><a href="{{url('about')}}">About</a></li>
				<li><a href="{{url('gallery')}}">Wedding Albums</a></li>
				<li><a href="{{url('contactus')}}">Contact</a></li>
			</ul>
            <div class="w3_agileits_social_media">
				<ul>
					
					<li><a href="#" class="wthree_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#" class="wthree_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#" class="wthree_dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					<li><a href="#" class="wthree_behance"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
				</ul>
			</div>

			<div class="copy-right">
				<p> 
                    <strong>Created with <i class="fa fa-heart" aria-hidden="true"></i> By <a href="http://shobarjonnoweb.com/">Shobarjonnoweb.com</a></strong> 
                </p>
			</div>
		</div>
	</div>