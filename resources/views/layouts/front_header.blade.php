<header>
		<div class="container">
			<!-- nav -->
			<nav class="navbar navbar-inverse">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				<div class="logo">
					<h1><a href="{{url('/')}}"><span></span> <div class="logo-img"><img src="{{asset('public/images/logo_c.png')}}" alt="Logo"/></div><p class="sub_title">RED ELEGANCE</p> </a></h1>
				</div>	
				</div>
<?php 
//use 
    $weddings = \DB::table('services')->where('service_type_id',1)->get();
	$corporates = \DB::table('services')->where('service_type_id',2)->get();
	$birthdays = \DB::table('services')->where('service_type_id',3)->get();
	$ticketings = \DB::table('services')->where('service_type_id',4)->get();
	//return $weddings;

 ?>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				  <nav class="cl-effect-13" id="cl-effect-13">
						<ul class="nav navbar-nav">
							<li class="menu-item active"><a href="{{url('/')}}">Home</a></li>
							<li class="menu-item"><a href="{{url('about')}}">About</a></li>
							<li class="menu-item dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Service</a>
								<ul class="dropdown-menu agile_short_dropdown">
									<li class="dropdown-submenu">
										<a class="dropdown-toggle" data-toggle="dropdown" href="{{url('service_category/2')}}">Corporate</a>
										<ul class="dropdown-menu">
										@foreach($corporates as $corp)
											<li><a href="{{url('service/'.$corp->id)}}">{{ $corp->service_title }}</a></li>
										@endforeach
										</ul>
									</li>
									<li class="menu-item dropdown-submenu">
										<a class="dropdown-toggle" data-toggle="dropdown" href="{{url('service_category/1')}}">Wedding</a>
										<ul class="dropdown-menu">
											@foreach($weddings as $wed)
											<li><a href="{{url('service/'.$wed->id)}}">{{ $wed->service_title }}</a></li>
										@endforeach
										</ul>
									</li>
									<li class="menu-item dropdown-submenu">
										<a class="dropdown-toggle" data-toggle="dropdown" href="{{url('service_category/4')}}">Ticketing</a>
										<ul class="dropdown-menu">
											@foreach($ticketings as $tick)
											<li><a href="{{url('service/'.$tick->id)}}">{{ $tick->service_title }}</a></li>
										@endforeach
										</ul>
									</li>
									<li class="menu-item dropdown-submenu">
										<a class="dropdown-toggle" data-toggle="dropdown" href="{{url('service_category/3')}}">Bithday</a>
										<ul class="dropdown-menu">
											@foreach($birthdays as $bir)
											<li><a href="{{url('service/'.$bir->id)}}">{{ $bir->service_title }}</a></li>
										@endforeach
										</ul>
									</li>
								</ul>
							</li>
							<li><a href="{{ url('allpackage') }}">Packages</a></li>
							<li><a href="{{ url('gallery') }}">Gallery</a></li>
							<li><a href="{{ url('contactus') }}">Contact Us</a></li>
						</ul>
						
					</nav>

				</div>
				<!-- /.navbar-collapse -->
			  </div>
			  <!-- /.container-fluid -->
			</nav> 
			<!-- //nav -->
		</div>
	</header>