<!DOCTYPE html>
<html lang="zxx">
<head>
<title>Red Elegance</title>    
<link href="{{ asset('public/css/frontend/font-awesome.css') }}" rel="stylesheet" type="text/css" media="all" /><!-- fontawesome css --> 
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">    
{{-- <link href="{{ asset('public/css/frontend/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" /><!-- Bootstrap stylesheet --> --}}
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link href="{{ asset('public/css/frontend/snow.css') }}" rel="stylesheet" type="text/css" media="all" /><!-- stylesheet -->
<link href="{{ asset('public/css/frontend/style.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('public/css/frontend/lsb.css') }}" rel="stylesheet" type="text/css" media="all" />

<!-- stylesheet -->




<!-- meta tags -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Wedding" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //meta tags -->
<!--fonts-->
<link href="//fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700" rel="stylesheet">
<!--//fonts-->
</head>
<body>
    
<!-- header -->
	@include('layouts.front_header')	
	<div class="">
		 <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul>
                                    <li>{!! Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                            @if (Session::has('info'))
                                <div class="alert alert-info alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <ul>
                                        <li>{!! Session::get('info') !!}</li>
                                    </ul>
                                </div>
                            @endif
                        @if (Session::has('warning'))
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul>
                                    <li>{!! Session::get('warning') !!}</li>
                                </ul>
                            </div>
                        @endif
                            @if (Session::has('danger'))
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <ul>
                                        <li>{!! Session::get('danger') !!}</li>
                                    </ul>
                                </div>
                            @endif
                    </div>
                </div>
	</div>
	<!-- //header -->
	<!-- banner-slider -->
	@yield('content')
    
	<!-- footer -->
  @include('layouts.front_footer')
	<!-- //footer -->

	<script type="text/javascript" src="{{ asset('public/js/frontend/jquery-2.1.4.min.js') }}"></script><!-- Required-js -->
	<script src="{{ asset('public/js/frontend/bootstrap.min.js') }}"></script><!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="{{ asset('public/js/frontend/bootstrap_sidemenu.js') }}"></script>
	<script src="{{ asset('public/js/frontend/responsiveslides.min.js') }}"></script>
							<script>
								// You can also use "$(window).load(function() {"
								$(function () {
								  // Slideshow 4
								  $("#slider3").responsiveSlides({
									auto: true,
									pager:true,
									nav:false,
									speed: 500,
									namespace: "callbacks",
									before: function () {
									  $('.events').append("<li>before event fired.</li>");
									},
									after: function () {
									  $('.events').append("<li>after event fired.</li>");
									}
								  });
							
								});
							 </script>
							 <!-- js -->

	<!-- //main slider-banner --> 	
	<!-- Countdown-Timer-JavaScript -->
			<script src="js/simplyCountdown.js"></script>
			<script>
				var d = new Date(new Date().getTime() + 948 * 120 * 120 * 2000);

				// default example
				simplyCountdown('.simply-countdown-one', {
					year: d.getFullYear(),
					month: d.getMonth() + 1,
					day: d.getDate()
				});

				// inline example
				simplyCountdown('.simply-countdown-inline', {
					year: d.getFullYear(),
					month: d.getMonth() + 1,
					day: d.getDate(),
					inline: true
				});

				//jQuery example
				$('#simply-countdown-losange').simplyCountdown({
					year: d.getFullYear(),
					month: d.getMonth() + 1,
					day: d.getDate(),
					enableUtc: false
				});
			</script>
		<!-- //Countdown-Timer-JavaScript -->
 <!-- required-js-files-->
							<link href="{{asset('public/css/frontend/owl.carousel.css')}}" rel="stylesheet">
							    <script src="{{asset('public/js/frontend/owl.carousel.js')}}"></script>
							        <script>
							    $(document).ready(function() {
							      $("#owl-demo").owlCarousel({
							        items :5,
									itemsDesktop : [768,4],
									itemsDesktopSmall : [414,3],
							        lazyLoad : true,
							        autoPlay : true,
							        navigation :true,
									
							        navigationText :  false,
							        pagination : true,
									
							      });
                                    
                                $("#owl-demo2").owlCarousel({
							        items :5,
									itemsDesktop : [768,4],
									itemsDesktopSmall : [414,3],
							        lazyLoad : true,
							        autoPlay : true,
							        navigation :true,
									
							        navigationText :  false,
							        pagination : true,
									
							      });
								  
							    });
							    </script>
								 <!--//required-js-files-->

	<!-- here stars scrolling icon -->
			<script type="text/javascript">
				$(document).ready(function() {
					/*
						var defaults = {
						containerID: 'toTop', // fading element id
						containerHoverID: 'toTopHover', // fading element hover id
						scrollSpeed: 1200,
						easingType: 'linear' 
						};
					*/
										
					$().UItoTop({ easingType: 'easeOutQuart' });
										
					});
			</script>
			<!-- start-smoth-scrolling -->
			<script type="text/javascript" src="{{ asset('public/js/frontend/move-top.js') }}"></script>
			<script type="text/javascript" src="{{ asset('public/js/frontend/easing.js') }}"></script>
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					$(".scroll").click(function(event){		
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
					});
				});
			</script>
			<!-- start-smoth-scrolling -->
	<!-- //here ends scrolling icon -->	
</body>
</html>