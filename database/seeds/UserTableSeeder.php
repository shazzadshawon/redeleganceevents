<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        [
            'name' => 'Admin',
            'email' =>'admin@gmail.com',
            'password' => bcrypt('admin'),
        ]
      
        );
        
        DB::table('users')->insert(
        [
            'name' => 'Admin AGV',
            'email' =>'admin@agvcorp.biz',
            'password' => bcrypt('adminagv'),
        ]
      
        );

        DB::table('users')->insert(
        [
            'name' => 'User',
            'email' => 'user@gmail.com',
            'password' => bcrypt('user'),
        ]
      
        );  

        DB::table('users')->insert(
        [
            'name'  => 'Akash',
            'email' => 'akash@gmail.com',
            'password' => bcrypt('akash'),
        ]
      
        );

        
       
    }
}
