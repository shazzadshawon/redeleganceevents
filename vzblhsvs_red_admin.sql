-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 09, 2017 at 03:06 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vzblhsvs_red_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `about_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `about_image`, `about_description`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Corporate Decoration/Stall/Fair', NULL, NULL),
(2, NULL, 'Grand Opening ceremony', NULL, NULL),
(3, NULL, 'Corporate Program & Seminar', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_type` int(11) DEFAULT NULL,
  `cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `cat_type`, `cat_status`, `created_at`, `updated_at`) VALUES
(1, 'name2', 2, '1', NULL, NULL),
(2, 'new', 1, '1', NULL, NULL),
(3, 'demo', 2, '1', NULL, NULL),
(4, 'Cat-2', 1, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `contact_title` longtext COLLATE utf8_unicode_ci,
  `contact_email` longtext COLLATE utf8_unicode_ci,
  `contact_phone` longtext COLLATE utf8_unicode_ci,
  `contact_description` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `contact_title`, `contact_email`, `contact_phone`, `contact_description`, `created_at`, `updated_at`) VALUES
(1, 'Test', '01918278373', '01918278373', NULL, '2017-07-22 20:08:30', '2017-07-22 20:08:30'),
(3, 'Anindita Sarker', 'aninditasarker73@gmail.com', '01747320533', 'Dear Sir,\r\n\r\nI am a passout student of MBA now i am looking for a job if u want send my cv.', '2017-08-18 06:12:42', '2017-08-18 06:12:42'),
(4, NULL, 'jparsons@blogpros.com', '8057651717', NULL, '2017-11-18 09:13:44', '2017-11-18 09:13:44');

-- --------------------------------------------------------

--
-- Table structure for table `eventcategories`
--

CREATE TABLE `eventcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_cat_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `gallery_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_image_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `gallery_image`, `gallery_image_status`, `created_at`, `updated_at`) VALUES
(7, '1506508101.jpg', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2017_07_17_113813_create_categories_table', 1),
(16, '2017_07_17_113838_create_subcategories_table', 1),
(17, '2017_07_17_113916_create_sliders_table', 1),
(18, '2017_07_17_113940_create_events_table', 1),
(19, '2017_07_17_113959_create_teams_table', 1),
(20, '2017_07_17_114018_create_reviews_table', 1),
(21, '2017_07_17_114045_create_galleries_table', 1),
(22, '2017_07_17_114101_create_abouts_table', 1),
(23, '2017_07_17_114121_create_services_table', 1),
(24, '2017_07_18_035856_create_eventcategories_table', 1),
(27, '2017_07_18_042138_create_packages_table', 2),
(28, '2017_07_18_042154_create_packagecategories_table', 2),
(29, '2017_07_18_055256_create_servicetypes_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `offer_description` longtext,
  `offer_start` longtext,
  `offer_end` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `offer_description`, `offer_start`, `offer_end`, `created_at`, `updated_at`) VALUES
(1, '<p>Duis aute irure dolor reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Lorem Duis aute irure dolor reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', '01 January, 1970 12:00 am', '01 January, 1970 12:00 am', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packagecategories`
--

CREATE TABLE `packagecategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_subtitle` longtext COLLATE utf8mb4_unicode_ci,
  `package_description` longtext COLLATE utf8mb4_unicode_ci,
  `package_include` longtext COLLATE utf8mb4_unicode_ci,
  `package_exclude` longtext COLLATE utf8mb4_unicode_ci,
  `cat_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package_name`, `package_subtitle`, `package_description`, `package_include`, `package_exclude`, `cat_id`, `created_at`, `updated_at`) VALUES
(6, 'Bhutan', 'The Authentic Bhutan', '<p>3 nights &amp; 4 days Tour package)</p>\r\n\r\n<p>Thimpu: 2 nights</p>\r\n\r\n<p>Paro: 1 night &nbsp;&nbsp;</p>\r\n\r\n<p>Terms &amp; Condition:</p>\r\n\r\n<ul>\r\n	<li>Only For Bangladeshi Passport Holder</li>\r\n	<li>Rate Can Be Change Without Prior Notice</li>\r\n	<li>Package Can Be Customized</li>\r\n</ul>', '<ol>\r\n	<li>Return Air Ticket</li>\r\n	<li>Accommodation in twin share basis 2 or 3 or 4 star</li>\r\n</ol>\r\n\r\n<p>Hotel / Resort</p>\r\n\r\n<ol>\r\n	<li>Everyday Sightseeing &amp; Transportation</li>\r\n	<li>Air Port Drop &amp; Pickup</li>\r\n	<li>All permits and Tax included</li>\r\n	<li>Licensed English Speaking Tour Guide</li>\r\n	<li>No Hidden Cost</li>\r\n</ol>', '<p>Meals, beverages, telephone, laundry, insurance or other extra personal effects.</p>', NULL, NULL, NULL),
(7, 'Bhutan', 'The Authentic Bhutan', '<p>5 nights &amp; 6 days Tour package)</p>\r\n\r\n<p>Thimpu: 3 nights</p>\r\n\r\n<p>Paro: 1 night</p>\r\n\r\n<p>Punakha: 1 night</p>\r\n\r\n<p>Terms &amp; Condition:</p>\r\n\r\n<ul>\r\n	<li>Only For Bangladeshi Passport Holder</li>\r\n	<li>Rate Can Be Change Without Prior Notice</li>\r\n	<li>Package Can Be Customized</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', '<ol>\r\n	<li>Return Air Ticket</li>\r\n	<li>Accommodation in twin share basis 2 or 3 or 4 star</li>\r\n</ol>\r\n\r\n<p>Hotel / Resort</p>\r\n\r\n<ol>\r\n	<li>Everyday Sightseeing &amp; Transportation</li>\r\n	<li>Air Port Drop &amp; Pickup</li>\r\n	<li>All permits and Tax included</li>\r\n	<li>Licensed English Speaking Tour Guide</li>\r\n	<li>No Hidden Cost</li>\r\n</ol>', '<p>Meals, beverages, telephone, laundry, insurance or other extra personal effects.</p>', NULL, NULL, NULL),
(8, 'Nepal', 'A kingdom in The Sky', '<p>3 nights &amp; 4 days Tour package)</p>\r\n\r\n<p>Kathmandu: 2 nights</p>\r\n\r\n<p>Nagarkot: 1 night</p>\r\n\r\n<p>Terms &amp; Condition:</p>\r\n\r\n<ul>\r\n	<li>Only For Bangladeshi Passport Holder</li>\r\n	<li>Rate Can Be Change Without Prior Notice</li>\r\n	<li>Package Can Be Customized</li>\r\n</ul>', '<ol>\r\n	<li>Return Air Ticket</li>\r\n	<li>Accommodation in twin share basis 2 or 3 or 4 or 5 star</li>\r\n</ol>\r\n\r\n<p>Hotel / Resort</p>\r\n\r\n<ol>\r\n	<li>Everyday Sightseeing &amp; Transportation</li>\r\n	<li>Air Port Drop &amp; Pickup</li>\r\n	<li>All permits and Tax included</li>\r\n	<li>Licensed English Speaking Tour Guide</li>\r\n	<li>No Hidden Cost</li>\r\n</ol>', '<p>Meals, beverages, telephone, laundry, insurance or other extra personal effects</p>', NULL, NULL, NULL),
(9, 'Nepal', 'A kingdom in The Sky', '<p>4 nights &amp; 5 days Tour package)</p>\r\n\r\n<p>Kathmandu: 2 nights</p>\r\n\r\n<p>Nagarkot: 1 night</p>\r\n\r\n<p>Pokhara: 1 night</p>\r\n\r\n<p>Terms &amp; Condition:</p>\r\n\r\n<ul>\r\n	<li>Only For Bangladeshi Passport Holder</li>\r\n	<li>Rate Can Be Change Without Prior Notice</li>\r\n	<li>Package Can Be Customized</li>\r\n</ul>', '<ol>\r\n	<li>Return Air Ticket</li>\r\n	<li>Accommodation in twin share basis 2 or 3 or 4 or 5 star</li>\r\n</ol>\r\n\r\n<p>Hotel / Resort</p>\r\n\r\n<ol>\r\n	<li>Everyday Sightseeing &amp; Transportation</li>\r\n	<li>Air Port Drop &amp; Pickup</li>\r\n	<li>All permits and Tax included</li>\r\n	<li>Licensed English Speaking Tour Guide</li>\r\n	<li>No Hidden Cost</li>\r\n</ol>', '<p>Meals, beverages, telephone, laundry, insurance or other extra personal effects</p>', NULL, NULL, NULL),
(10, 'Thailand', 'Luxurious Bangkok & Pattaya', '<p>4 nights &amp; 5 days Tour package)</p>\r\n\r\n<p>Bangkok: 2 nights</p>\r\n\r\n<p>Pattaya: 2 nights</p>\r\n\r\n<p>Terms &amp; Condition:</p>\r\n\r\n<ul>\r\n	<li>Only For Bangladeshi Passport Holder</li>\r\n	<li>Rate Can Be Change Without Prior Notice</li>\r\n	<li>Package Can Be Customized</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', '<ol>\r\n	<li>Return Air Ticket</li>\r\n	<li>Accommodation in twin share basis 2 or 3 or 4 or 5</li>\r\n</ol>\r\n\r\n<p>star Hotel / Resort</p>\r\n\r\n<ol>\r\n	<li>Everyday Sightseeing &amp; Transportation</li>\r\n	<li>Air Port Drop &amp; Pickup</li>\r\n	<li>All permits and Tax included</li>\r\n	<li>Licensed English Speaking Tour Guide</li>\r\n	<li>No Hidden Cost</li>\r\n</ol>', '<p>Meals, Visa, beverages, telephone, laundry, insurance or other extra personal effects.</p>', NULL, NULL, NULL),
(11, 'malaysia', 'Discover The Amazing Malaysia', '<p>4 nights &amp; 5 days Tour package)</p>\r\n\r\n<p>Kuala lumpur: 2 nights</p>\r\n\r\n<p>Langkawi: 2 nights</p>\r\n\r\n<p>Terms &amp; Condition:</p>\r\n\r\n<ul>\r\n	<li>Only For Bangladeshi Passport Holder</li>\r\n	<li>Rate Can Be Change Without Prior Notice</li>\r\n	<li>Package Can Be Customized</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', '<ol>\r\n	<li>Return Air Ticket</li>\r\n	<li>Accommodation in twin share basis 2 or 3 or 4 or 5 star</li>\r\n</ol>\r\n\r\n<p>Hotel / Resort</p>\r\n\r\n<ol>\r\n	<li>Everyday Sightseeing &amp; Transportation</li>\r\n	<li>Air Port Drop &amp; Pickup</li>\r\n	<li>All permits and Tax included</li>\r\n	<li>Licensed English Speaking Tour Guide</li>\r\n	<li>No Hidden Cost</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>', '<p>Meals, Visa, beverages, telephone, laundry, insurance or other extra personal effects</p>', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `review_title`, `review_description`, `review_image`, `review_status`, `created_at`, `updated_at`) VALUES
(8, 'Review 4', '<p><img alt=\"\" src=\"/public/templateEditor/kcfinder/upload/images/avatar-mini2.jpg\" style=\"height:35px; width:35px\" />This is a demo review 4</p>', '1500713667.jpg', '1', '30 November, 2017', NULL),
(9, 'Review 2', '<p>Demo&nbsp;review 2</p>', '1500713867.jpg', '1', '22 July, 2017', NULL),
(10, 'Review 1', '<p>trbtyhnyun 65u6n5u5u</p>', '1500714088.jpg', '1', '22 July, 2017', NULL),
(11, 'Review 3', '<p>tbt 6yh67 65yuh65yun 656un6un 77jmuyjmuy hjujymnu drfewr sdcse ki,lk,il&nbsp;</p>', '1500714250.jpg', '1', '22 July, 2017', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(19, 'Band Party', '1506150152.jpg', '<p>Are you looking for Band Party service in Dhaka, Bangladesh for your Marriage Ceremony, Birthday, Wedding, Business opening, Rag Day or any other desired party or celebration? Then, you are at right place. We can provide you all facilities with Funny, Entertaining people with their resources that will make your program enjoyable.</p>\r\n\r\n<p>We provide Band Party&nbsp;Service in Gulshan, Banani, Uttara, Dhanmondi, Mohakhali, Baridhara, Nikunja, Mirpur, Mohammadpur, Khilgaon, Shyamoli, Nikunja, Rampura, Khilkhet,Mohakhali DOHS, Baridhara DOHS,Old Dhaka, Tejgaon, Cantonment, Badda, Palton, Wari. You can also contact us if you are form outside of those area and we will help you to serve you.</p>\r\n\r\n<p>We have 6, 8 and 10 member&rsquo;s team for band party service.</p>\r\n\r\n<p>&nbsp;</p>', NULL, 1, '1', NULL, NULL),
(20, 'Chotpoti, Fuchka, Pitha & Coffee Stall', '1506150578.jpg', '<p>We provide Chotpoti, Fuchkha, Pitha &amp; Coffee stall for any kind of event. we cover<br />\r\n<br />\r\n*Wedding &amp; Holud<br />\r\n*Fair<br />\r\n*Picnic<br />\r\n*Corporate event<br />\r\n*Business event etc<br />\r\n<br />\r\nCall us for more details....</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 5500 bdt (per item)</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: Chotpoti fuhcka coffee pitha service for wedding in Dhaka, Chotpoti fuchkha service provider in Bangladesh, Chotpoti fuchka for wedding event in dhaka.</p>\r\n\r\n<p>&nbsp;</p>', NULL, 1, '1', NULL, NULL),
(21, 'Photography & Cinematography', '1506150860.jpg', '<p>Khandany (Co-partner of Red Elegance) is a key photography solution for your elegant programs such as Weddings, Corporate events, Product photography and enjoyable family moments. We have a bunch of efficient , dedicated and highly experienced photographers. We want to give you the very best according to your budget taste and expectation. We believe our pursuit is to photograph your beautiful moments to keep them remarkable forever. We are waiting for capturing your moments, Call us now.</p>\r\n\r\n<p><br />\r\n:: Photography packages ::<br />\r\n<br />\r\nKhandany 101<br />\r\nPrice: 5500 (Promotional Offer)<br />\r\nNumber of Photographer: 01 (One senior photographer)<br />\r\nDuration: 4.30 hour (1 Event)<br />\r\nNumber of Capture: Unlimited (The photographer will cover decoration, venue, portraits, couple shots, family photo, and group photo)<br />\r\nLight setup: Umbrella portrait Light<br />\r\nDelivery: Soft copies will be delivered in a nicely illustrated DVD.<br />\r\nPrints : 4R=100 copy (Matt or Glossy paper)<br />\r\n<br />\r\nKhandany 102<br />\r\nPrice: 8000(Promotional Offer)<br />\r\nNumber of Photographer: 02 (Two senior photographer )<br />\r\nDuration: 4.30 hour (1 Event)<br />\r\nNumber of Capture: Unlimited (The photographer will cover decoration, venue, portraits, couple shots, family photo, guest and group photo)<br />\r\nLight setup: Umbrella portrait light<br />\r\nDelivery: Soft copies will be delivered in a nicely illustrated DVD.<br />\r\nPrints: 4R=120 copy (Matt or Glossy paper), 12L=1 copy<br />\r\n<br />\r\nKhandany 103<br />\r\nPrice: 12000<br />\r\nNumber of Photographer: 03(Three senior Photographer)<br />\r\nDuration: 5.00 hour (1 Event)<br />\r\nNumber of Capture: Unlimited (The photographer will cover decoration, venue, portraits, couple shots, family photo, guest, relatives, moments and group photo)<br />\r\nLight setup: Umbrella portrait light, soft box etc.<br />\r\nDelivery: Soft copies will be delivered in a nicely illustrated DVD.<br />\r\nPrints: 4R=150 copy (Matt or Glossy paper), 12L=2 copy<br />\r\n<br />\r\nKhandany 104<br />\r\nPrice: 16000<br />\r\nNumber of Photographer: 04 (Four senior Photographer)<br />\r\nDuration: 5.00 hour (1 Event)<br />\r\nNumber of Capture: Unlimited(The photographer will cover decoration, venue, portraits, couple shots, family photo, guest, relatives, moments and group photo)<br />\r\nLight setup: Umbrella portrait light, soft box, octagon etc.<br />\r\nDelivery: Soft copies will be delivered in a nicely illustrated DVD</p>\r\n\r\n<p>Prints: 4R=200 copy (Matt or Glossy paper), 12L=2 copy<br />\r\n<br />\r\nKhandany Signature 101<br />\r\nPrice: 25000<br />\r\nNumber of Photographer: 05 (Four senior photographer &amp; one extra roaming photographer)<br />\r\nDuration: 5.00 hour (1 Event)<br />\r\nNumber of Capture: Unlimited (The photographer will cover decoration, venue, portraits, couple shots, family photo, guest, relatives, moments and group photo)<br />\r\nLight: Umbrella light setup soft box ,octagon, snoot, strip box, gradient filter etc.<br />\r\nDelivery: Soft copies will be delivered in a nicely illustrated DVD<br />\r\nPrints: 4R =200 copy (Matt or Glossy paper), 12L=2 copy, 20L =1 copy<br />\r\nOne pasting album<br />\r\nFree Pre wedding or Post wedding shooting.<br />\r\n<br />\r\nKhandany Corporate<br />\r\nPrice: 8000<br />\r\nNumber of Photographer: 02 (Two senior Photographer )<br />\r\nDuration: 4.00 hour (1 Event)<br />\r\nNumber of Capture : Unlimited<br />\r\nLight : Umbrella Light setup ,Soft box etc<br />\r\nDelivery: Soft copies will be delivered in a nicely illustrated DVD<br />\r\nDelivery within 12 hours by google drive for press release<br />\r\nPrints: On demand<br />\r\n<br />\r\nKhandany Family Photoshoot (outdoor)<br />\r\nPrice: 5000<br />\r\nNumber of Photographer: 01(one senior photographer )<br />\r\nDuration: 2.30 hour (outdoor Shoot)<br />\r\nNumber of Capture: Unlimited<br />\r\nDelivery: Soft copies will be delivered in DVD.<br />\r\nPrints : 4R=50 copy (Matt or Glossy paper), 12L=1 copy<br />\r\n<br />\r\n::Cinematography Packages::<br />\r\nKhandany 201<br />\r\nPrice: 6000<br />\r\nCinematographer: 01<br />\r\nDuration: 4.30 hour (1 Event)<br />\r\nRecording quality: Full HD 1080 (1920*1080) with DSLR camera<br />\r\nLight: Umbrella Light setup, Video Special lights<br />\r\nThe music will be of your selection.<br />\r\nDelivery: Full video (20-30 minutes) with trailer (3-5minutes) will be delivered in DVD.<br />\r\ncinematography available starts from 10000 to 50000 tk<br />\r\n<br />\r\n&nbsp;</p>\r\n\r\n<p>Gear List<br />\r\n::::::::::::::::::::::::::<br />\r\nBody<br />\r\nCanon 5D Mark 3<br />\r\nCanon 6D<br />\r\nCanon 7D mark 2<br />\r\nLens<br />\r\n16-35mm F2.8 L<br />\r\n24-70mm F2.8 L<br />\r\n24-105mm F4 L IS<br />\r\n70-200mm F 2.8 L IS 2<br />\r\n85mm F1.8<br />\r\n50mm F1.4<br />\r\nSpeed light<br />\r\nCanon 430 ex 2<br />\r\nCanon 600 ex rt<br />\r\nUmbrella ,soft box ,octagon and other necessary setup.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: wedding photography&nbsp; in Dhaka. Wedding holud photography and cinematography in Bangladesh, wedding cinematography in Dhaka, wedding photography package in Bangladesh.</p>', NULL, 1, '1', NULL, NULL),
(22, 'Palki Rent Service', '1506151015.jpg', '<p>Palki or Palanquin bears the true resemblance of our age old tradition. It served as a luxury transport, mainly for the women which with time faded away with the influx of improved automobiles. Our line of Doli are mostly hired for different wedding purposes, irrespective of all communities, immersions purposes and other social events. All our Doli are designed in the most elegant and stylish manner using the premium quality materials, fabrics and designer accessories.</p>\r\n\r\n<p>We have Different types of palki collection.</p>\r\n\r\n<ul>\r\n	<li>Raj-ghonshi Palki</li>\r\n	<li>Moyurponkhi Palki</li>\r\n	<li>Khola Palki</li>\r\n	<li>Traditional Palki</li>\r\n	<li>Khola Palki</li>\r\n	<li>Flower palki</li>\r\n</ul>\r\n\r\n<p>Cost: <strong>Starting from 6500 bdt</strong></p>\r\n\r\n<p>Tag: Wedding palki rent in Dhaka, palki rental service in Bangladesh, traditional palki rent provider in Bangladesh. Palki rent service in Dhaka.</p>', NULL, 1, '1', NULL, NULL),
(23, 'TomTom / Ghorar Gari', '1506151117.jpg', '<p>We provide Ghorar Gari / TomTom for wedding. Also you can use ghorar gari for marketing, product promotion and shooting.</p>\r\n\r\n<p><strong>Types of Event we cover with Ghorar Gari:</strong></p>\r\n\r\n<ol>\r\n	<li>Marriage or Wedding Ceremony for Bride and Groom</li>\r\n	<li>For New Business Launch</li>\r\n	<li>Poster, Flyer Distribution</li>\r\n	<li>Political Purpose like showdown</li>\r\n	<li>Just for recreation</li>\r\n</ol>\r\n\r\n<p>Currently we are providing Ghorar Gari service in Gulshan, Banani, Uttara, Dhanmondi, Mohakhali, Baridhara, Nikunja, Mirpur, Mohammadpur, Khilgaon, Shyamoli, Nikunja, Rampura, Khilkhet. You can also contact us if you are form outside of those area and we will help you to serve you.</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 8000 bdt</strong></p>\r\n\r\n<p>Tag: Tomtom rent service in Dhaka. Ghorar gari rent in Bangladesh, Wedding Tomtom service in Dhaka.</p>', NULL, 1, '1', NULL, NULL),
(24, 'Elephant Rent Service', '1506151504.jpg', '<p><img alt=\"Description: 17690497_1697283130298064_929253434_n.jpg\" src=\"file:///C:\\Users\\AdminAGV\\AppData\\Local\\Temp\\msohtmlclip1\\01\\clip_image001.jpg\" style=\"height:229.5pt; width:345pt\" /></p>\r\n\r\n<p>We provide Elephant / Hati rental service for wedding. You can also use Elephant for marketing, product promotion, corporate event and shooting.</p>\r\n\r\n<p><strong>Types of Event we cover with Ghorar Gari:</strong></p>\r\n\r\n<ol>\r\n	<li>Marriage or Wedding Ceremony for Bride and Groom</li>\r\n	<li>For New Business Launch</li>\r\n	<li>Poster, Flyer Distribution</li>\r\n	<li>Political Purpose like showdown</li>\r\n	<li>Just for recreation</li>\r\n</ol>\r\n\r\n<p>Currently we are providing Ghorar Gari service in Gulshan, Banani, Uttara, Dhanmondi, Mohakhali, Baridhara, Nikunja, Mirpur, Mohammadpur, Khilgaon, Shyamoli, Nikunja, Rampura, Khilkhet. You can also contact us if you are form outside of those area and we will help you to serve you.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 25000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Tag: Hati or Elphant rental service in Dhaka, Elephant rent service in Bangladesh, Elephant rental service&nbsp; provider in Bangladesh.</strong></p>', NULL, 1, '1', NULL, NULL),
(25, 'Sound System & Light:', '1506151760.jpg', '<p>We have high range Orginal JBL, QW4, SRX, SUB etc sound box collection.<br />\r\nWe also have Disco LED lighting system.</p>\r\n\r\n<p>&quot;RED ELEGANCE&quot; have their own personal Lighting System and Original American JBL &amp; QW4 HIGH RANGE Sound System.</p>\r\n\r\n<p>We cover,</p>\r\n\r\n<ul>\r\n	<li>corporate program</li>\r\n	<li>seminar</li>\r\n	<li>Holud and mehedi night</li>\r\n	<li>wedding program</li>\r\n	<li>picnic</li>\r\n	<li>school program</li>\r\n	<li>islamic program</li>\r\n	<li>cultural program</li>\r\n	<li>live concert</li>\r\n	<li>Live Dj</li>\r\n	<li>Puja</li>\r\n	<li>Fashion show<br />\r\n	etc....<br />\r\n	<br />\r\n	Call for booking us now...<br />\r\n	RED ELEGANCE EVENTS</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 4000 bdt</strong></p>\r\n\r\n<p><strong>Tag: Sound system for holud wedding in Dhaka, Wedding sound box rent in Bangladesh,&nbsp; sound system for wedding event in Dhaka.</strong></p>', NULL, 1, '1', NULL, NULL),
(26, 'DJ  SHOW', '1506151934.jpg', '<p>WE arrange full DJ show with Best quality Sound System and Disco Lighting System. We provide Original JBL and QW4 sound with monitor and LED, lazer light, shimar and smoke setup....</p>\r\n\r\n<p>we mainly organize</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Wedding / Haldi night / Mehedi nights</li>\r\n	<li>Birthday show</li>\r\n	<li>Picnic show</li>\r\n	<li>Corporate show</li>\r\n	<li>class parties</li>\r\n	<li>Reunion</li>\r\n	<li>Corporate Football/Cricket league</li>\r\n	<li>Any Cultural Program</li>\r\n	<li>Product Marketing Promotion</li>\r\n	<li>Fashion Show<br />\r\n	and other programs.....</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>our packages are Starting from given BDT....<br />\r\n1. normal<br />\r\n2 Silver<br />\r\n3. Premium<br />\r\n4. Gold<br />\r\nWe also provide Female DJ and Sound System<br />\r\nus for more details....</p>\r\n\r\n<p>RED ELEGANCE EVENTS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 8000 bdt</strong></p>\r\n\r\n<p><strong>Tag: Dj show for wedding in Dhaka, Dj party organizer in Bangladesh, wedding dj event in Dhaka.</strong></p>', NULL, 1, '1', NULL, NULL),
(27, 'Band Artist & Singer', '1506152026.jpg', '<p>WE arrange full professional band concert show professional male &amp; female artist with Best quality sound system and disco lighting system. we provide JBL and QW4 sound with monitor and LED lazer light, shimar and smoke setup....</p>\r\n\r\n<p>We have professinal male and female artist.<br />\r\nwe mainly organize&nbsp;<br />\r\n*Wedding progm<br />\r\n*birthday show<br />\r\n*picnic show<br />\r\n*corporate show<br />\r\n*class parties<br />\r\n*cultural show<br />\r\n*reunion<br />\r\nand other programs.....</p>\r\n\r\n<p>our packages are<br />\r\n1. normal<br />\r\n2 Silver&nbsp;<br />\r\n3. Premium&nbsp;<br />\r\n4. Gold&nbsp;<br />\r\nWe also provide female DJ and Sound system<br />\r\ncall us for more details....</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 22000 bdt</strong></p>\r\n\r\n<p><strong>Tag: band artist and singer for wedding program in Dhaka, wedding singer artist and band show in Bangladesh, female Singer for wedding event in Dhaka.</strong></p>\r\n\r\n<p>&nbsp;</p>', NULL, 1, '1', NULL, NULL),
(28, 'Wedding Ornaments', '1506152312.jpg', '<p>We provide wedding ornament for bride. We have large number of different collection of ornaments sets. We can give a traditional looks to bride with our ornaments.</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 2000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Tag: wedding ornament provider in Dhaka, wedding ornament service in Dhaka.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', NULL, 1, '1', NULL, NULL),
(29, 'Dancer Group', '1506152515.jpg', '<p>We have professional dancer. They are experienced and well dancer. You can hire a dancer group by us. You can hire our dancer group for any type of program. All dancers are educated, smart, and handsome.<br />\r\nFollowing program we performed</p>\r\n\r\n<ul>\r\n	<li>Wedding stage perform</li>\r\n	<li>Corporate show</li>\r\n	<li>Fashion show</li>\r\n	<li>Cultural program</li>\r\n	<li>Picnic</li>\r\n	<li>Opening ceremony</li>\r\n	<li>Birthday party</li>\r\n	<li>Mehedi Program</li>\r\n	<li>Home party etc,<br />\r\n	&nbsp;</li>\r\n</ul>\r\n\r\n<p>Currently We are providing service in Gulshan, Banani, Uttara, Dhanmondi, Mohakhali, Baridhara, Nikunja, Mirpur, Mohammadpur, Khilgaon, Shyamoli, Nikunja, Rampura, Khilkhet. You can also contact us if you are form outside of those area and we will help you to serve you.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 10000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Tag: Female dancer group for wedding event in Dhaka. Dancer group in Dhaka, Dancer for weddinh program in Bangladesh. Professional Dancer group in Dhaka.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', NULL, 1, '1', NULL, NULL),
(30, 'Kawali Song', '1506152610.jpg', '<p>Qawwali is a very old form of music that is mostly popular in southern part of Asia. It is a very important part of the musical tradition. It is sung during any auspicious occasion as well as in pious location. Artist On Call arranges for the best Qawwali singers in Dhaka for making the events pure and auspicious.</p>\r\n\r\n<p>Sufi music</p>\r\n\r\n<p>Bollywood Qawwali</p>\r\n\r\n<p>Regional Qawwali</p>\r\n\r\n<p>Traditional Qawwali</p>\r\n\r\n<p>Gazzal</p>\r\n\r\n<p>Call us for the Best Service in the Town.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;<strong>Cost: </strong><strong>Starting from 25000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Tag: Kawali or qawwali singer in Dhaka, kawali or qawwali artist in Bangladesh, kawali band artist in Dhaka.</strong></p>\r\n\r\n<p>&nbsp;</p>', NULL, 1, '1', NULL, NULL),
(31, 'Bashor Ghor Decoration', '1506152743.jpg', '<p>Bashor Ghor is the most important part of wedding program. Every bride groom wants their Bashor Ghor will be beautiful and memorable. We will make you bashor ghor look like heaven. We make it so beautiful, and sweet.<br />\r\nWe decorate Bashor Ghor with flower, Candle, Balloons and lighting. We have large number of unique design and ideas. Also you can customize your design. Contact us, to get the best Bshor Ghor Decoration Service in Dhaka, Bangladesh.</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 5000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Tag: bashor ghor decoration in Dhaka. Bashor ghor decoration in Bangladesh, bashor ghor decorator in Dhaka.</strong></p>', NULL, 1, '1', NULL, NULL),
(32, 'Wedding Car Decoration', '1506152813.jpg', '<p>Wedding car is one of the most important part of marriage ceremony. That&rsquo;s why, we got lot&rsquo;s of Car design Ideas and Designs to make your car look attractive and unique. Just call us to get best wedding car decoration service in Dhaka and we will give you best design and ideas&nbsp;according to your budget and plan.</p>\r\n\r\n<p>Currently we are providing Wedding car decoration service in Gulshan, Banani, Uttara, Dhanmondi, Mohakhali, Baridhara, Nikunja, Mirpur, Mohammadpur, Khilgaon, Shyamoli, Nikunja, Rampura, Khilkhet. You can also contact us if you are form outside of those area and we will help you to serve you.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 5000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Tag: Wedding car decoration in Dhaka. Wedding holud car decoration in Bangladesh, Flower car decoration in Dhaka.</strong></p>\r\n\r\n<p>&nbsp;</p>', NULL, 1, '1', NULL, NULL),
(33, 'Corporate Event Arrangements', '1506154875.jpg', '<p><a href=\"http://www.showsinabox.com/how-to-plan-your-next-successful-corporate-event.html\">Corporate event</a> can be defined as a gathering that is sponsored by a business for its employees, business partners, clients and/or prospective clients. These events can be for larger audiences such as conventions or smaller events like conferences, meetings or holiday parties. We are excellent in arranging such these types of corporate events.</p>\r\n\r\n<p>&nbsp;</p>', NULL, 3, '1', NULL, NULL),
(34, 'Seminar', '1506155052.jpg', '<p>A Business seminar, sometimes referred to as a conference, is a commercial program where attendees are given information or training. It is usually held for groups of 10-50 individuals and is frequently held at a <a href=\"https://www.thebalance.com/how-to-negotiate-with-a-hotel-1223819\">hotel meeting space</a>, an academic institution, or within an office conference room. Some of the most popular kinds of seminars in business focus on personal development and business strategies.&nbsp;</p>\r\n\r\n<p>These are a few <a href=\"https://www.thebalance.com/corporate-events-common-types-1223785\">common types of seminars</a> you might encounter in the business and academic worlds and we will help you out to arranging these types&nbsp; of seminar without any hassle.&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: Corporate seminar in Dhaka, seminar organizer, corporate seminar arrangement</p>', NULL, 3, '1', NULL, NULL),
(35, 'ticketing 1', '1506496203.jpg', '<p>Ticket text</p>', NULL, 4, '1', NULL, NULL),
(39, 'Product launching or Opening ceremony', '1506503694.jpg', '<p>Business firms should use an opening ceremony to help open their business or highlight their product launch with customers. Opening ceremonies for business functions help call attention to products and services by combining entertainment and informative pieces about the business. A strong grand opening ceremony is as much about creating a buzz about the business as developing long-term prospects. We have different ideas and entertaining tools for creative product launching or opening ceremony.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: Product launching in Bangladesh, Grand opening, Shop opening ceremony, new product launch.</p>', NULL, 3, '1', NULL, NULL),
(40, 'Convocation & Farewell Parties:', '1506503989.jpg', '<p>One of the most prestigious formal events at any university or college is the convocation ceremony. At the convocation ceremony, the graduating batch students are formally awarded their degrees. This day is significant for the students as they pass one phase of their lives and enter the next. It is significant for the institution as on this day they declare the completion of education of their students and send them to make their careers in the world.</p>\r\n\r\n<p>As this day is held in high regard by the student community, institute as well as the media and outside observers, it is important to project an image of highest standard of professionalism. Perk Events&#39; provide our expert convocation event planning, management, theme design and direction, staging, audio - visual, lights &amp; sound, security and catering services for convocation events, in addition to arranging the academic regalia, stationary as well as all specific requirements.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: convocation organizer in&nbsp; Dhaka, convocation event management in Dhaka.</p>', NULL, 3, '1', NULL, NULL),
(41, 'Trade Fair & Stall decoration', '1506504608.jpg', '<p>If you are looking to organize a trade fair or a trade expo, Red Elegance Events is the best event management company to suit your event management needs. Whether you are organizing a mobile expo, auto expo, book fair, Developer Company or any other kind of trade fair, we can bring together the perfect set of companies &amp; organizers to participate in the event, and make the event a grand success. At the same time, we can provide our world class event management services including event marketing, media management, promotion to consumers, audio-visual and production services as well as all event management needs.</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 4000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: Trade fair stall in Dhaka, trade fair organizer in Bangladesh, exhibition and trade fair in Dhaka.</p>\r\n\r\n<p>&nbsp;</p>', NULL, 3, '1', NULL, NULL),
(42, 'Corporate Photography & Cinematography:', '1506504709.jpg', '<p>When you are organizing an event, you want to get good photographs taken of the whole event, along with a well shot video. Event Videography &amp; Photography is done not only for preserving the memories of the wonderful events, but also for documentation preparation, online promotion of the event, creating multimedia of the event, pitching to future sponsors as well as for distribution to the media.</p>\r\n\r\n<p>Our best event videography (cinematography) and event photography service not only includes the camera work, but also art direction of the whole event. Our art team prepares scripts and guides for your speakers, hosts, key guests and other participants according to which very beautiful theatrically cinematographed videos can be produced. We make sure that your event looks stunningly beautiful on camera.</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 5500 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: Corporate Photography in Dhaka, corporate videography in Bangladesh, Corporate Video and photography Promotion.</p>', NULL, 3, '1', NULL, NULL),
(43, 'Stage Set Design & Decoration', '1506504779.jpg', '<p>Red Elegance Events has the best art design team which carefully analyzes the theme and atmosphere for the event and creates beautiful set designs for the events. We work on not only the event stage design, but also on the styling of the complete event venue whether it is inside or outside the premises. We shall provide you multiple stage design &amp; event theme choices for your event including the structure of the stage, audio-visual choices for the event and lighting choices from which you can chose your preferred styling.</p>\r\n\r\n<p>Our expert team of professionals shall not only work on providing you the best design &amp; styling choices, but also work on producing the design sets with the best and strongest materials &amp; deliver it for the event. For us, building the set of your imagination is only a matter of your own choice. Once you give us the go ahead, we use beautiful and stunning visual media, fine art skills &amp; colorful imagery to build the most beautiful set designs for your event. All our stages are tried and tested for safety and your guests witness the best set designs with the highest saftey standards ensured.</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 10000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: Stage set design in Dhaka, corporate stage design in Bangladesh, corporate event stage set designer in Bangladesh, corporate stage decoration in&nbsp; Dhaka.</p>\r\n\r\n<p>&nbsp;</p>', NULL, 3, '1', NULL, NULL),
(44, 'Live Concert & Band Artist', '1506504967.jpg', '<p>We can provide all of the music for your wedding, from the ceremony, to the reception, right through to your evening entertainment. Or perhaps you are looking for music for a family reunion, a Birthday, Dinner or Anniversary Party, Corporate Event Entertainment or for any other Celebration or Event.</p>\r\n\r\n<p>We can Provide you Professional / TV Artists For:</p>\r\n\r\n<ul>\r\n	<li><strong>Live Bands</strong><strong> &ndash; Modern Band Music, Solo Artist, Metal Band Music, Live DJ, Saxophone, Guitars Solos, Swing, Modern &amp; Traditional Jazz Bands all ideal for background music, dinner, drinks reception, wedding music or for any other party.</strong></li>\r\n	<li><strong>Live Ghazal &amp; Kawali </strong><strong>&ndash; with spectacular ghazal Singer with tradition musicians like Tabla, Flute, Harmonium etc. For classical &amp; semi classical family, corporate &amp; private events the possibility of a fully sophisticated music Listeners.</strong></li>\r\n	<li><strong>Live Classical Music </strong><strong>- Sitaar a string quartet &amp; a Flute Synthesis - all ideal for background music, dinner, drinks reception, wedding music or for any other party, celebration or corporate event.</strong></li>\r\n</ul>\r\n\r\n<p>We can entertain your guests in a style to suit your setting whether it is in an intimate space, such as a room in your house with perhaps a live jazz or ghazal or classical music.</p>\r\n\r\n<p>Whatever the situation or event - your guests will always remember the music that we will play!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost: </strong><strong>Starting from 20000 bdt</strong></p>\r\n\r\n<p>Tag; Concert organizer in Dhaka, live concert organizer in Bangladesh, Concert event organizer.</p>', NULL, 3, '1', NULL, NULL),
(45, 'Birthday Stage & Balloon Decoration', '1506505782.jpg', '<p>Red Elegance Event&rsquo;s Balloon Decorating Service is designed to help you turn your party into an unforgettable event. Whether you are entertaining a few children at home, organising a dinner at a restaurant or hotel, hosting a product launch for thousands, balloon decorations are guaranteed to put your guests in the party mood.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Over the years we have specialised in live events and have worked for clients ranging from BBC television to the Royal family. Balloon releases for the Millennium and balloon drops on live television. We have also become specialists in &#39;balloon lifts&#39; enabling people to &#39;fly&#39; using balloons.</p>\r\n\r\n<p>Our Balloon Decorating team are willing and able to produce almost anything you require. If you would like to discuss our Balloon Decorating Service, please contact us.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost:</strong><strong> Starting From 8000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: birthday stage decoration in Bangladesh, Balloon decoration in Dhaka, birthday balloon decoration in Dhaka, Balloon decoration service in Dhaka.</p>\r\n\r\n<p>&nbsp;</p>', NULL, 2, '1', NULL, NULL),
(46, 'Mascot Doll', '1506505897.jpg', '<h1>We provide Mascot doll or Cartoon doll rental serice for any kind of event.<br />\r\nas like<br />\r\n*Birthday event<br />\r\n*Corporate event<br />\r\n*Business promotion<br />\r\n*Wedding event<br />\r\n*Fair<br />\r\netc</h1>\r\n\r\n<p>We different cartoon character collection</p>\r\n\r\n<ul>\r\n	<li>Doremon</li>\r\n	<li>micky mouse</li>\r\n	<li>angry bird</li>\r\n	<li>Tom &amp; Jerry</li>\r\n	<li>Donald Duck<br />\r\n	etc</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost:</strong><strong> Starting From 5500 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: Mascot doll rent in Bangladesh, Mascot doll cartoon doll retanl in Dhaka, birthday Doll in Dhaka, Mascot cartoon character doll service in Dhaka.</p>', NULL, 2, '1', NULL, NULL),
(47, 'Magic Show', '1506505990.jpg', '<p>Red Elegance Events provide on demand magician for Birthday Party. Kids love magic, that&rsquo;s why, a magician can make a birthday party much more funny, enjoyable and remember able. Our magicians are experienced and perform in different event like in Wedding, Birthday Party, Fare, Business Program. If you are looking for magician in Bangladesh, Click on Order Now and submit your Name, Phone Number and Address and we will contact you.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost:</strong><strong> Starting From 10000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: Magic show in Bangladesh, Magic show and magician in Dhaka, Magic show for birthday&nbsp; in Dhaka, Magician service in Dhaka.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', NULL, 2, '1', NULL, NULL),
(48, 'Kids Play Zone', '1506506098.jpg', '<p>Red Elegance is the most trusted supplier of kids play zone or soft play and other extensive and ever growing range of activities like trampolines parks, rope course, rock climbing, zip lines, wave slides, tunnels roller sliders and lot more. We also have off shelf soft play products from single items up to large multi-piece play sets and play zones, covering an ever range of themes.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost:</strong><strong> Starting From 10000 bdt</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tag: Kids play zone service in Bangladesh, Soft play zone service in Dhaka, birthday kids play zone in Dhaka, Kids play ground service in Dhaka.</p>\r\n\r\n<p>&nbsp;</p>', NULL, 2, '1', NULL, NULL),
(49, 'Bio Scope', '1506506253.jpg', '<p>The bio scope is oldest name of cinema. it&#39;s mean it&#39;s a box with many eyes , from where people can watch still movie inside of box , like picture&#39;s .&nbsp; where - now a days, cinema is very high tech with air condition auditorium , but people and kids they wanna gather in&nbsp; any special occasion in any traveling place&nbsp; or&nbsp; any exhibition &nbsp; or in park around the country . actually bio scope is still nursing&nbsp; tradition&nbsp; , memory , heritage also lot of thinks for each country . about my country we can watch normally in bio scope&nbsp; &quot; dadir chokhe jol (the tear of&nbsp; grand ma) , rohimoner prem(love of rahimon) , momotajer paitta jay &quot; ,&nbsp; also they shown &quot;billaler zari (zari song of billal) &quot;&nbsp; . sometimes cartoon picture&nbsp; , sometimes shown sexy dance , cock fighting etc.<br />\r\nbut we have to remember- Before the&nbsp; television and the Internet, here was Bio scope only way To entertainment - bring images of one&#39;s favorite movie stars to the neighborhood - peppered with music from the Talkies themselves.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost:</strong> Starting from 5500 bdt</p>\r\n\r\n<p>&nbsp;</p>', NULL, 2, '1', NULL, NULL),
(50, 'Shap khela', '1506506322.jpg', '<h1>We provide traditional Shap khela for any kind of event.... such as</h1>\r\n\r\n<h1>*Wedding progm<br />\r\n*Corporate progm<br />\r\n*Business promotion<br />\r\n*Cultural progm<br />\r\n*Puja<br />\r\n*Fair<br />\r\netc</h1>\r\n\r\n<h1>call us for booking<br />\r\nRed Elegance Events</h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost:</strong> Starting from 5500 bdt</p>\r\n\r\n<p>&nbsp;</p>', NULL, 2, '1', NULL, NULL),
(51, 'Banor Nach', '1506506415.jpg', '<h1>We provide traditional Banor nach (moneky dance) for any kind of event.... such as</h1>\r\n\r\n<h1>*Wedding progm<br />\r\n*Corporate progm<br />\r\n*Business promotion<br />\r\n*Cultural progm<br />\r\n*Puja<br />\r\n*Fair<br />\r\netc</h1>\r\n\r\n<h1>call us for booking<br />\r\nRed Elegance Events</h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost:</strong> Starting from 5500 bdt</p>\r\n\r\n<p>&nbsp;</p>', NULL, 2, '1', NULL, NULL),
(52, 'Nagor Dola & Grameen Mela', '1506506520.jpg', '<h1>We provide traditional Nagor Dola &amp; Grameen mela for any kind of event.... such as</h1>\r\n\r\n<h1>*Wedding progm<br />\r\n*Corporate progm<br />\r\n*Business promotion<br />\r\n*Cultural progm<br />\r\n*Puja<br />\r\n*Fair<br />\r\netc</h1>\r\n\r\n<h1>call us for booking<br />\r\nRed Elegance Events</h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cost:</strong> Starting from 10000 bdt</p>\r\n\r\n<p>&nbsp;</p>', NULL, 2, '1', NULL, NULL),
(53, 'Air ticketing domestic', '1506506920.jpg', '<p>We provide ticketing as well as full range of travel-related services for both corporate and individual travel. We have all of the Bangladeshi Airlines own capping for inbound any Air Ticket.</p>\r\n\r\n<p>Red Elegance is committed to provide all domestic air tickets at a reasonable price. We can arrange cheapest air tickets for any country from the most popular airlines in Bangladesh such as:</p>\r\n\r\n<p><strong>Routes we cover: (Both one way &amp; return)</strong></p>\r\n\r\n<p>Dhaka-Chittagong-Dhaka</p>\r\n\r\n<p>Dhaka-Cox&#39;s Bazar-Dhaka</p>\r\n\r\n<p>Dhaka-Sylhet-Dhaka</p>\r\n\r\n<p>Dhaka-Saidpur-Dhaka</p>\r\n\r\n<p>Dhaka-Rajshahi-Dhaka</p>\r\n\r\n<p>Dhaka-Barisal-Dhaka</p>\r\n\r\n<p>Dhaka-Jessore-Dhaka&nbsp;</p>', NULL, 4, '1', NULL, NULL),
(54, 'Air Ticketing International', '1506507002.jpg', '<p>Red Elegance is one of the best air ticketing agents in Dhaka, we are committed to provide all International Air Tickets at a reasonable price. Our international air ticketing services are quick, potent and crack as we stay and re-check for air tickets, air fares, etc.</p>\r\n\r\n<p>We can arrange cheapest air tickets for any country from the most popular airlines in the world such as:&nbsp;</p>\r\n\r\n<p><strong>Routes we cover: (Both one way &amp; return)</strong></p>\r\n\r\n<p>Dhaka-Bangkok-Dhaka</p>\r\n\r\n<p>Dhaka-Kolkata-Dhaka</p>\r\n\r\n<p>Dhaka-Kathmandu-Dhaka</p>\r\n\r\n<p>Dhaka-Kuala lumpur-Dhaka</p>\r\n\r\n<p>Dhaka-Singapore-Dhaka</p>\r\n\r\n<p>Dhaka-Bali-Dhaka</p>\r\n\r\n<p>Dhaka-Yangon-Dhaka</p>\r\n\r\n<p>Dhaka-Delhi-Dhaka</p>\r\n\r\n<p>Dhaka-Bhutan-Dhaka</p>\r\n\r\n<p>Dhaka-Maldives</p>\r\n\r\n<p>Kong-Dhaka</p>\r\n\r\n<p>Dhaka-New York-Dhaka</p>\r\n\r\n<p>Dhaka-Australia-Dhaka</p>\r\n\r\n<p>Dhaka-London-Dhaka</p>\r\n\r\n<p>&nbsp;</p>', NULL, 4, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `servicetypes`
--

CREATE TABLE `servicetypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `servicetypes`
--

INSERT INTO `servicetypes` (`id`, `type_name`, `created_at`, `updated_at`) VALUES
(1, 'Wedding', NULL, NULL),
(2, 'Birthday', NULL, NULL),
(3, 'Corporate', NULL, NULL),
(4, 'Ticketing', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_subtitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `slider_image`, `slider_title`, `slider_subtitle`, `slider_status`, `created_at`, `updated_at`) VALUES
(3, '1502553085.jpg', 'We Make Your Dream Come True', NULL, '1', NULL, NULL),
(5, '1502553109.jpg', 'Your Event Patner', NULL, '1', NULL, NULL),
(6, '1502553394.jpg', 'We Make Your Dream Come True', NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `sub_cat_name`, `sub_cat_status`, `service_type`, `created_at`, `updated_at`) VALUES
(3, 'sub cat 1', '1', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `team_title`, `team_description`, `team_image`, `team_status`, `created_at`, `updated_at`) VALUES
(8, 'Md. Tanvir Karim', '(Managing Director)\r\nRed Elegance Events\r\nEmail: tanvirkarim18@yahoo.com\r\nMob: +8801670706912', '1507980501.jpg', '1', NULL, NULL),
(9, 'Md. Nazrul islam (Shanto)', '(Operation Manager)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com\r\nMob: +8801680683049', '1507981228.jpg', '1', NULL, NULL),
(10, 'Mamur Rahman Taj', '(Creative Officer)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com', '1507981317.jpg', '1', NULL, NULL),
(11, 'Asif Rahman (Auntor)', '(Marketing Executive)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com', '1502553300.jpg', '1', NULL, NULL),
(12, 'Md. Rashedul Islam (Rasel)', '(Finance Officer)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com', '1507981719.jpg', '1', NULL, NULL),
(13, 'Monoarul Hasan (Saif)', '(Travels & Air Ticketing)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com', '1507981780.jpg', '1', NULL, NULL),
(14, 'Sabrina Rahman', '(Brand Promoter)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com', '1507981955.jpg', '1', NULL, NULL),
(15, 'Saidul Masud (Rishad)', '(Brand Promoter & Official DJ)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com', '1507982019.jpg', '1', NULL, NULL),
(16, 'Mahbubur Rahman (Nibir)', '(Brand Promoter & Official DJ)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com', '1507982099.jpg', '1', NULL, NULL),
(17, 'Evan Ahmed', '(Official Photographer)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com', '1507982160.jpg', '1', NULL, NULL),
(18, 'Ashiqur Rahman (Ashik)', '(Official Photographer)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com', '1507982331.jpg', '1', NULL, NULL),
(19, 'Asif Hossain', '(Official Photographer)\r\nRed Elegance Events\r\nEmail: red_elegance@yahoo.com', '1507982472.jpg', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$36UNCJVp0rA0UURa3leOiebSOKhgqjAticgs9BquGRH1uEv6LB/SS', 'IIkjsrZsbq5qJIm80RDmNkOybgaGfgcFcPXxVztP0995rRRiBnjh1NetCVUB', NULL, NULL),
(2, 'Admin AGV', 'admin@agvcorp.biz', '$2y$10$IhzYjNrAxUQY6GlO4ZiAzuUtwquN7a.UB1ly2sA.QrFRESp6betX6', NULL, NULL, NULL),
(3, 'Red Elegance', 'admin@redeleganceevents.com', '$2y$10$qLXA1AXUT50b2RFKD1HMx.iaPvbVRH6E02zVzqZui5KHQ/Kr2sd3W', 'QeVAUwNyWhgQYmuv6ZG4COlbNc1ARlEmqJ1TTLUh7bkXUiBb3RilZuBkrz6j', NULL, NULL),
(4, 'Akash', 'akash@gmail.com', '$2y$10$yVMVbg/0ERbvhNQEiVI8TOVgUBa7sGAMYqzXPTbMw7UH.LuFvv0NS', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `counter` int(11) DEFAULT '0',
  `daily_count` int(11) DEFAULT '0',
  `dhaka` int(11) DEFAULT '0',
  `chittagong` int(11) DEFAULT '0',
  `barisal` int(11) DEFAULT '0',
  `khulna` int(11) DEFAULT '0',
  `mymensingh` int(11) DEFAULT '0',
  `rajshahi` int(11) DEFAULT '0',
  `rangpur` int(11) DEFAULT '0',
  `sylhet` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `counter`, `daily_count`, `dhaka`, `chittagong`, `barisal`, `khulna`, `mymensingh`, `rajshahi`, `rangpur`, `sylhet`, `created_at`, `updated_at`) VALUES
(1, 1046, 10, 185, 3, 0, 0, 0, 0, 0, 2, NULL, '17-12-09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventcategories`
--
ALTER TABLE `eventcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packagecategories`
--
ALTER TABLE `packagecategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicetypes`
--
ALTER TABLE `servicetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `eventcategories`
--
ALTER TABLE `eventcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `packagecategories`
--
ALTER TABLE `packagecategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `servicetypes`
--
ALTER TABLE `servicetypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
