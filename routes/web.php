<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

//Route::get('/index', 'HomeController@index');
Route::get('/', 'HomeController@home');
Route::get('/about', 'HomeController@about');
Route::get('/service/{id}', 'HomeController@service');
Route::get('/service_category/{id}', 'HomeController@service_cat');
Route::get('/allpackage', 'HomeController@allpackage');
Route::get('/package/{id}', 'HomeController@singlepackage');
Route::get('/gallery', 'HomeController@gallery');
Route::get('/contactus', 'HomeController@contactus');
Route::post('/storecontact', 'HomeController@storecontact');
//Route::post('/form','HomeController@upload');


// Backend routes Start

//Route::get(['middleware' => 'auth'], function () {
Route::group(['middleware' => 'auth'], function(){
    
			Route::get('/shobarjonnoweb', 'DashboardController@index');


			// // Service category
			// Route::get('/categories', 'CategoryController@index');
			// Route::get('/addcategory', 'CategoryController@add');
			// Route::post('/storecategory', 'CategoryController@store');
			// Route::get('/editcategory/{id}', 'CategoryController@edit');
			// Route::post('/updatecategory/{id}', 'CategoryController@update');
			// Route::get('/deletecategory/{id}', 'CategoryController@destroy');


			// Service subcategory
			Route::get('/subcategories', 'SubcategoryController@index');
			Route::get('/addsubcategory', 'SubcategoryController@add');
			Route::post('/storesubcategory', 'SubcategoryController@store');
			Route::get('/editsubcategory/{id}', 'SubcategoryController@edit');
			Route::post('/updatesubcategory/{id}', 'SubcategoryController@update');
			Route::get('/deletesubcategory/{id}', 'SubcategoryController@destroy');

			// Service 
			Route::get('/services', 'ServiceController@index');
			Route::get('/singleservice/{id}', 'ServiceController@view');
			Route::get('/addservice', 'ServiceController@add');
			Route::post('/storeservice', 'ServiceController@store');
			Route::get('/editservice/{id}', 'ServiceController@edit');
			Route::post('/updateservice/{id}', 'ServiceController@update');
			Route::get('/deleteservice/{id}', 'ServiceController@destroy');
    
			Route::get('/service_callback', 'ServiceController@service_callback');


			// package

			Route::get('/packages', 'PackageController@index');
			Route::get('/addpackage', 'PackageController@add');
			Route::post('/storepackage', 'PackageController@store');
			Route::get('/editpackage/{id}', 'PackageController@edit');
			Route::post('/updatepackage/{id}', 'PackageController@update');
			Route::get('/deletepackage/{id}', 'PackageController@destroy');


			// // package category
			Route::get('/categories', 'CategoryController@index');
			Route::get('/addcategory', 'CategoryController@add');
			Route::post('/storecategory', 'CategoryController@store');
			Route::get('/editcategory/{id}', 'CategoryController@edit');
			Route::post('/updatecategory/{id}', 'CategoryController@update');
			Route::get('/deletecategory/{id}', 'CategoryController@destroy');

			// // Slider
			Route::get('/sliders', 'SliderController@index');
			Route::get('/addslider', 'SliderController@add');
			Route::post('/storeslider', 'SliderController@store');
			Route::get('/editslider/{id}', 'SliderController@edit');
			Route::post('/updateslider/{id}', 'SliderController@update');
			Route::get('/deleteslider/{id}', 'SliderController@destroy');


			// // Gallery
			Route::get('/galleries', 'GalleryController@index');
			Route::get('/addgallery', 'GalleryController@add');
			Route::post('/storegallery', 'GalleryController@store');
			Route::get('/editgallery/{id}', 'GalleryController@edit');
			Route::post('/updategallery/{id}', 'GalleryController@update');
			Route::get('/deletegallery/{id}', 'GalleryController@destroy');


			// // Team
			Route::get('/teammembers', 'TeamController@index');
			Route::get('/viewteammember/{id}', 'TeamController@show');
			Route::get('/addteammember', 'TeamController@add');
			Route::post('/storeteammember', 'TeamController@store');
			Route::get('/editteammember/{id}', 'TeamController@edit');
			Route::post('/updateteammember/{id}', 'TeamController@update');
			Route::get('/deleteteammember/{id}', 'TeamController@destroy');


			// // Review
			Route::get('/reviews', 'ReviewController@index');
			// Route::get('/viewreview/{id}', 'ReviewController@show');
			Route::get('/addreview', 'ReviewController@add');
			Route::post('/storereview', 'ReviewController@store');
			Route::get('/editreview/{id}', 'ReviewController@edit');
			Route::post('/updatereview/{id}', 'ReviewController@update');
			Route::get('/deletereview/{id}', 'ReviewController@destroy');



			// // About
			Route::get('/abouts', 'AboutController@index');
			// Route::get('/viewreview/{id}', 'ReviewController@show');
			Route::get('/addabout', 'AboutController@add');
			Route::post('/storeabout', 'AboutController@store');
			Route::get('/editabout/{id}', 'AboutController@edit');
			Route::post('/updateabout/{id}', 'AboutController@update');
			Route::get('/deleteabout/{id}', 'AboutController@destroy');


			// // cont
			Route::get('/messages', 'ContactController@index');

			Route::get('/deletecontact/{id}', 'ContactController@destroy');

			// // offer
			Route::get('/editoffer', 'OfferController@edit');

			Route::post('/updateoffer', 'OfferController@update');



});

