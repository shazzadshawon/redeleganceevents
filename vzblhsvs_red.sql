-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for vzblhsvs_red
CREATE DATABASE IF NOT EXISTS `vzblhsvs_red` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `vzblhsvs_red`;

-- Dumping structure for table vzblhsvs_red.abouts
CREATE TABLE IF NOT EXISTS `abouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `about_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.abouts: ~5 rows (approximately)
/*!40000 ALTER TABLE `abouts` DISABLE KEYS */;
INSERT INTO `abouts` (`id`, `about_image`, `about_description`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Corporate Decoration/Stall/Fair', NULL, NULL),
	(2, NULL, 'Grand Opening ceremony', NULL, NULL),
	(3, NULL, 'Corporate Program & Seminar', NULL, NULL),
	(4, NULL, 'Road Show / Rally', NULL, NULL),
	(5, NULL, 'Birthday Party / Wedding Program', NULL, NULL);
/*!40000 ALTER TABLE `abouts` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_type` int(11) DEFAULT NULL,
  `cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.categories: ~4 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `cat_name`, `cat_type`, `cat_status`, `created_at`, `updated_at`) VALUES
	(1, 'name2', 2, '1', NULL, NULL),
	(2, 'new', 1, '1', NULL, NULL),
	(3, 'demo', 2, '1', NULL, NULL),
	(4, 'Cat-2', 1, '1', NULL, NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.contacts
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_title` longtext COLLATE utf8_unicode_ci,
  `contact_email` longtext COLLATE utf8_unicode_ci,
  `contact_phone` longtext COLLATE utf8_unicode_ci,
  `contact_description` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table vzblhsvs_red.contacts: ~0 rows (approximately)
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` (`id`, `contact_title`, `contact_email`, `contact_phone`, `contact_description`, `created_at`, `updated_at`) VALUES
	(1, 'Test', '01918278373', '01918278373', NULL, '2017-07-22 21:08:30', '2017-07-22 21:08:30');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.eventcategories
CREATE TABLE IF NOT EXISTS `eventcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_cat_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.eventcategories: ~0 rows (approximately)
/*!40000 ALTER TABLE `eventcategories` DISABLE KEYS */;
/*!40000 ALTER TABLE `eventcategories` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.events
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.events: ~0 rows (approximately)
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.galleries
CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gallery_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_image_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.galleries: ~5 rows (approximately)
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` (`id`, `gallery_image`, `gallery_image_status`, `created_at`, `updated_at`) VALUES
	(1, '1500523876.jpg', '1', NULL, NULL),
	(2, '1500529904.jpg', '1', NULL, NULL),
	(3, '1500530116.jpg', '1', NULL, NULL),
	(4, '1500530127.jpg', '1', NULL, NULL),
	(6, '1500530371.jpg', '1', NULL, NULL);
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.migrations: ~15 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(13, '2014_10_12_000000_create_users_table', 1),
	(14, '2014_10_12_100000_create_password_resets_table', 1),
	(15, '2017_07_17_113813_create_categories_table', 1),
	(16, '2017_07_17_113838_create_subcategories_table', 1),
	(17, '2017_07_17_113916_create_sliders_table', 1),
	(18, '2017_07_17_113940_create_events_table', 1),
	(19, '2017_07_17_113959_create_teams_table', 1),
	(20, '2017_07_17_114018_create_reviews_table', 1),
	(21, '2017_07_17_114045_create_galleries_table', 1),
	(22, '2017_07_17_114101_create_abouts_table', 1),
	(23, '2017_07_17_114121_create_services_table', 1),
	(24, '2017_07_18_035856_create_eventcategories_table', 1),
	(27, '2017_07_18_042138_create_packages_table', 2),
	(28, '2017_07_18_042154_create_packagecategories_table', 2),
	(29, '2017_07_18_055256_create_servicetypes_table', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.offers
CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_description` longtext,
  `offer_start` longtext,
  `offer_end` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table vzblhsvs_red.offers: ~0 rows (approximately)
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` (`id`, `offer_description`, `offer_start`, `offer_end`, `created_at`, `updated_at`) VALUES
	(1, '<p>Duis aute irure dolor reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Lorem Duis aute irure dolor reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', '05 July, 2017 12:00 am', '19 July, 2017 12:00 am', NULL, NULL);
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.packagecategories
CREATE TABLE IF NOT EXISTS `packagecategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `package_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.packagecategories: ~0 rows (approximately)
/*!40000 ALTER TABLE `packagecategories` DISABLE KEYS */;
/*!40000 ALTER TABLE `packagecategories` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.packages
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_include` longtext COLLATE utf8mb4_unicode_ci,
  `package_exclude` longtext COLLATE utf8mb4_unicode_ci,
  `cat_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.packages: ~0 rows (approximately)
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
INSERT INTO `packages` (`id`, `package_name`, `package_subtitle`, `package_description`, `package_include`, `package_exclude`, `cat_id`, `created_at`, `updated_at`) VALUES
	(3, 'Bhutan', '5 nights & 6 days Tour package', '<p><strong>Thimpu:</strong>&nbsp;3 nights</p>\r\n\r\n<p><strong>Paro:</strong>&nbsp;1 night</p>\r\n\r\n<p><strong>Punakha:&nbsp;</strong>1 night</p>', '<ol>\r\n	<li><strong>Return Air Ticket</strong></li>\r\n	<li><strong>Accommodation in twin share basis 2 or 3 or 4 star Hotel / Resort</strong></li>\r\n	<li><strong>Everyday Sightseeing &amp; Transportation</strong></li>\r\n	<li><strong>Air Port Drop &amp; Pickup</strong></li>\r\n	<li><strong>All permits and Tax included</strong></li>\r\n	<li><strong>Licensed English Speaking Tour Guide</strong></li>\r\n	<li><strong>No Hidden Cost</strong></li>\r\n</ol>', '<p>Meals, beverages, telephone, laundry, insurance or other extra personal effects</p>', 2, NULL, NULL);
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `review_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.reviews: ~4 rows (approximately)
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` (`id`, `review_title`, `review_description`, `review_image`, `review_status`, `created_at`, `updated_at`) VALUES
	(8, 'Review 4', '<p>This is a demo review 4</p>', '1500713667.jpg', '1', '22 July, 2017', NULL),
	(9, 'Review 2', '<p>Demo&nbsp;review 2</p>', '1500713867.jpg', '1', '22 July, 2017', NULL),
	(10, 'Review 1', '<p>trbtyhnyun 65u6n5u5u</p>', '1500714088.jpg', '1', '22 July, 2017', NULL),
	(11, 'Review 3', '<p>tbt 6yh67 65yuh65yun 656un6un 77jmuyjmuy hjujymnu drfewr sdcse ki,lk,il&nbsp;</p>', '1500714250.jpg', '1', '22 July, 2017', NULL);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.services: ~3 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
	(15, 'Arong', '1500523815.jpg', '<p>uj6h7u vedv</p>', 3, 3, '1', NULL, NULL),
	(16, 'Birthday Stage & Balloon Decoration', '1500634536.jpg', '<p>Red Elegance Event&rsquo;s Balloon Decorating Service is designed to help you turn your party into an unforgettable event. Whether you are entertaining a few children at home, organising a dinner at a restaurant or hotel, hosting a product launch for thousands, balloon decorations are guaranteed to put your guests in the party mood.</p>\r\n\r\n<p>Over the years we have specialised in live events and have worked for clients ranging from BBC television to the Royal family. Balloon releases for the Millennium and balloon drops on live television. We have also become specialists in &#39;balloon lifts&#39; enabling people to &#39;fly&#39; using balloons. Our Balloon Decorating team are willing and able to produce almost anything you require. If you would like to discuss our Balloon Decorating Service, please contact us.</p>\r\n\r\n<p>Starting From 8000 bdt</p>', 3, 2, '1', NULL, NULL),
	(17, 'Photography & Cinematography', '1500633934.jpg', '<p>Khandany (Co-partner of Red Elegance) is a key photography solution for your elegant programs such as Weddings, Corporate events, Product photography and enjoyable family moments. We have a bunch of efficient , dedicated and highly experienced photographers. We want to give you the very best according to your budget taste and expectation. We believe our pursuit is to photograph your beautiful moments to keep them remarkable forever.</p>\r\n\r\n<p>Photography packages</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1>Khandany 101</h1>\r\n\r\n<ul>\r\n	<li>Price: 5500 (Promotional Offer)</li>\r\n	<li>Number of Photographer: 01 (One senior photographer)</li>\r\n	<li>Duration: 4.30 hour (1 Event)</li>\r\n	<li>Number of Capture: Unlimited (The photographer will cover decoration, venue, portraits, couple shots, family photo, and group photo)</li>\r\n	<li>Light setup: Umbrella portrait Light</li>\r\n	<li>Delivery: Soft copies will be delivered in a nicely illustrated DVD</li>\r\n	<li>Prints : 4R=100 copy (Matt or Glossy paper)</li>\r\n</ul>\r\n\r\n<h1>Khandany 102</h1>\r\n\r\n<ul>\r\n	<li>Price: 8000 (Promotional Offer)</li>\r\n	<li>Number of Photographer: 02 (Two senior photographer )</li>\r\n	<li>Duration: 4.30 hour (1 Event)</li>\r\n	<li>Number of Capture: Unlimited (The photographer will cover decoration, venue, portraits, couple shots, family photo, and group photo)</li>\r\n	<li>Light setup: Umbrella portrait Light</li>\r\n	<li>Delivery: Soft copies will be delivered in a nicely illustrated DVD</li>\r\n	<li>Prints : 4R=120 copy (Matt or Glossy paper), 12L=1 copy</li>\r\n</ul>', NULL, 1, '1', NULL, NULL);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.servicetypes
CREATE TABLE IF NOT EXISTS `servicetypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.servicetypes: ~4 rows (approximately)
/*!40000 ALTER TABLE `servicetypes` DISABLE KEYS */;
INSERT INTO `servicetypes` (`id`, `type_name`, `created_at`, `updated_at`) VALUES
	(1, 'Wedding', NULL, NULL),
	(2, 'Birthday', NULL, NULL),
	(3, 'Corporate', NULL, NULL),
	(4, 'Ticketing', NULL, NULL);
/*!40000 ALTER TABLE `servicetypes` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.sliders
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slider_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_subtitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.sliders: ~2 rows (approximately)
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` (`id`, `slider_image`, `slider_title`, `slider_subtitle`, `slider_status`, `created_at`, `updated_at`) VALUES
	(3, '1500521336.jpg', 'Slider 2', 'Demo Subitle 2', '1', NULL, NULL),
	(5, '1500637013.jpg', 'Slider 2', 'subtitle 2', '1', NULL, NULL);
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.subcategories
CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sub_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.subcategories: ~0 rows (approximately)
/*!40000 ALTER TABLE `subcategories` DISABLE KEYS */;
INSERT INTO `subcategories` (`id`, `sub_cat_name`, `sub_cat_status`, `service_type`, `created_at`, `updated_at`) VALUES
	(3, 'sub cat 1', '1', 1, NULL, NULL);
/*!40000 ALTER TABLE `subcategories` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.teams
CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `team_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.teams: ~5 rows (approximately)
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` (`id`, `team_title`, `team_description`, `team_image`, `team_status`, `created_at`, `updated_at`) VALUES
	(3, 'Member 1', 'Executive', '1500541909.jpg', '1', NULL, NULL),
	(4, 'member 2', 'Executive', '1500541922.jpg', '1', NULL, NULL),
	(5, 'member 3', 'Executive', '1500541946.jpg', '1', NULL, NULL),
	(6, 'member 4', 'Executive', '1500541959.jpg', '1', NULL, NULL),
	(7, 'member 5', 'Executive', '1500541974.jpg', '1', NULL, NULL);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vzblhsvs_red.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'admin@gmail.com', '$2y$10$36UNCJVp0rA0UURa3leOiebSOKhgqjAticgs9BquGRH1uEv6LB/SS', 'IIkjsrZsbq5qJIm80RDmNkOybgaGfgcFcPXxVztP0995rRRiBnjh1NetCVUB', NULL, NULL),
	(2, 'Admin AGV', 'admin@agvcorp.biz', '$2y$10$IhzYjNrAxUQY6GlO4ZiAzuUtwquN7a.UB1ly2sA.QrFRESp6betX6', NULL, NULL, NULL),
	(3, 'User', 'user@gmail.com', '$2y$10$.rSw4TumCpfMHdaVw7808u9quytqWV5v3MzZ06clWb9oZPc67cQu2', 'QeVAUwNyWhgQYmuv6ZG4COlbNc1ARlEmqJ1TTLUh7bkXUiBb3RilZuBkrz6j', NULL, NULL),
	(4, 'Akash', 'akash@gmail.com', '$2y$10$yVMVbg/0ERbvhNQEiVI8TOVgUBa7sGAMYqzXPTbMw7UH.LuFvv0NS', NULL, NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table vzblhsvs_red.visitors
CREATE TABLE IF NOT EXISTS `visitors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `counter` int(11) DEFAULT '0',
  `daily_count` int(11) DEFAULT '0',
  `dhaka` int(11) DEFAULT '0',
  `chittagong` int(11) DEFAULT '0',
  `barisal` int(11) DEFAULT '0',
  `khulna` int(11) DEFAULT '0',
  `mymensingh` int(11) DEFAULT '0',
  `rajshahi` int(11) DEFAULT '0',
  `rangpur` int(11) DEFAULT '0',
  `sylhet` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table vzblhsvs_red.visitors: ~1 rows (approximately)
/*!40000 ALTER TABLE `visitors` DISABLE KEYS */;
INSERT INTO `visitors` (`id`, `counter`, `daily_count`, `dhaka`, `chittagong`, `barisal`, `khulna`, `mymensingh`, `rajshahi`, `rangpur`, `sylhet`, `created_at`, `updated_at`) VALUES
	(1, 5, 2, 1, 2, 1, 1, 0, 0, 0, 0, NULL, '17-08-12');
/*!40000 ALTER TABLE `visitors` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
